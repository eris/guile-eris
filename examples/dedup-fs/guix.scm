(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (gnu packages autotools)
 (gnu packages compression)
 (gnu packages linux)
 (gnu packages pkg-config))

(define-public erofs-utils
  (package
    (name "erofs-utils")
    (version "1.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "git://git.kernel.org/pub/scm/linux/kernel/git/xiang/erofs-utils.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07hvijq2hsn3gg1kb8abrfk23n83j57yx8kyv4wqgwhhvd30myjc"))))
    (build-system gnu-build-system)
    (inputs
     `(("lz4" ,lz4)
       ("util-linux" ,util-linux "lib")))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)
       ("pkg-config" ,pkg-config)))
    (home-page "https://git.kernel.org/pub/scm/linux/kernel/git/xiang/erofs-utils.git/")
    (synopsis "User-space tools for EROFS filesystem")
    (description "EROFS (Enhanced Read-Only File System) is a compressed,
read-only filesystem optimized for resource-scarce devices.  This package
provides user-space tools for creating EROFS filesystems.")
    (license license:gpl2+)))

(package
  (name "eris-dedup-fs-example")
  (version "0")
  (source #f)
  (build-system gnu-build-system)
  (native-inputs
   `(("zstd" ,zstd)
     ("squashfs-tools" ,squashfs-tools)
     ("erofs-utils" ,erofs-utils)))
  (home-page #f)
  (synopsis #f)
  (description #f)
  (license license:gpl3+))
