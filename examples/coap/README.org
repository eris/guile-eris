#+title: CoAP examples

* Server

The file [[server.scm]] implements a ERIS CoAP store that is accessible over TCP and stores blocks in an in-memory hash-table.

When getting blocks it introduces a latency of 0.01 seconds to simulate network latency.

Run it with:

#+begin_src
  guile server.scm
#+end_src

* Client

The file [[client.scm]] implements a client that:

1. Encodes 100MiB of pseudo-random content to a local ERIS CoAP store.
2. Decodes the content using a lookahead decoder.

The lookahead decoder makes multiple asynchronous requests to the CoAP store. By doing this we get similar performance as with a server that does not introduce any extra latency. We make decoding performance less latency-bound.

To test the client without the lookahead comment lines 108-111 and uncomment line 112.

The exact parameters (lookahead distance, number of requests, etc.) is a bit finiky and requires some more tweaking. Please test with you network connections and report back your findings!

***WARNING*** The client will by default write the decoded content to stdout. Run it with ~pv~ to get decoding bandwidth:

#+begin_src
  guile client.scm | pv > /dev/null
#+end_src
