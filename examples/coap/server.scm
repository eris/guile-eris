; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later


;; An example CoAP ERIS store

(use-modules (coap message)
	     (coap option)
	     (coap code)
	     ((coap tcp) #:prefix coap.tcp:)

	     (sodium generichash)

	     (fibers)
	     (fibers channels)

	     (web uri)

	     (srfi srfi-1)
	     (srfi srfi-69)
	     (ice-9 match)
	     (rnrs bytevectors))

;; An in-memory store, a hash table.
;; I don't think this is thread-safe. I wonder how it will blow-up...
(define store (make-hash-table bytevector=? hash))

(define (blake2b-256 bv)
  (crypto-generichash bv #:out-len 32))

(define (coap-message-uri-path msg)
  "Decode the URI path from options in a CoAP message"
  (filter-map
     ;; check if it is an uri-path option (number 11)
   (lambda (opt)
     (if (equal? (coap-option-number opt) 11)
	 (utf8->string (coap-option-value opt))
	 #f))
   (coap-message-options msg)))

(define (coap-message-uri-query msg)
  (car (filter-map
	(lambda (opt)
	  (if (equal? (coap-option-number opt) 15)
	      (coap-option-value opt)
	      #f))
	(coap-message-options msg))))

(define (handler socket msg)

  (unless (eof-object? msg)

    (format #t "MSG: ~a ~a\n"
	    (coap-message-code msg)
	    (coap-message-uri-path msg))

    (match (coap-message-uri-path msg)

      ((".well-known" "eris" "blocks")

       (cond
	((equal? (coap-message-code msg)
		 (make-coap-code 'get))

	 (let* ((ref (coap-message-uri-query msg))
		(block (hash-table-ref/default
			store ref #f)))

	   ;; introduce some latency
	   (sleep 0.01)

	   (if block
	       (coap.tcp:send
		socket
		(make-coap-message 'content
				   #:payload block
				   #:token (coap-message-token msg)))
	       ;; reply with 404
	       (coap.tcp:send socket
			      (make-coap-message
			       'not-found
			       #:token (coap-message-token msg))))))

	((equal? (coap-message-code msg)
		 (make-coap-code 'put))
	 
	 (let* ((block (coap-message-payload msg))
		(ref (blake2b-256 block)))

	   (hash-table-set! store ref block)

	   ;; repsond with created
	   (coap.tcp:send
	    socket
	    (make-coap-message 'created
			       #:token (coap-message-token msg)))))
	
	 (else
	  (coap.tcp:send socket
			 (make-coap-message
			  'not-found
			  #:token (coap-message-token msg))))))

      (_ (coap.tcp:send socket
			(make-coap-message
			 'not-found
			 #:token (coap-message-token msg)))))))

(run-fibers
 (lambda ()
   (format #t "Starting ERIS CoAP store...\n")
   (coap.tcp:run-server
    ;; connection handler
    (lambda (socket addr)
      (format #t "CONNECTION: ~a\n" addr)
      (cons socket handler)))))
