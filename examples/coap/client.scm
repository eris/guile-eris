; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules (eris)
	     (eris coap)
	     (eris cache)
	     (eris read-capability)
	     (eris lookahead)
	     (eris utils buffer)

	     ((coap tcp) #:prefix coap.tcp:)

	     (fibers)
	     (web uri)

	     (sodium stream)
	     (sodium generichash)

	     (srfi srfi-71) ; Extended let*
	     (srfi srfi-171) ; Transducers

	     (rnrs bytevectors)
	     (rnrs io ports))

;; An example of a CoAP client that uses a block cache.

;; This uses a lookahead decoder that optimizes decoding performance.

(define rstdout
  (case-lambda
    (() (standard-output-port))
    ((port bv) (put-bytevector port bv) port)
    ((port) (close-port port))))

(define null-nonce (make-bytevector 12 0))

(define* (pseudo-random-bytevector
	  length
	  #:key (seed "Encoding for Robust Immutable Storage"))
  (crypto-stream-chacha20-ietf
   #:clen length
   #:nonce null-nonce
   #:key (crypto-generichash (string->utf8 seed) #:out-len 32)))

(run-fibers
 (lambda ()

   ;; The ERIS CoAP Store URL
   (define store-url (string->uri "coap+tcp://localhost/.well-known/eris"))

   ;; Encode content and get read-capability
   (define read-capability
     ;; Use some SRFI-71 to get the right value
     (let ((read-capability
	    (eris-encode
	     ;; generate a 100MiB (pseudo) random bytevector
	     (pseudo-random-bytevector (* 1024 1024 100))
	     #:block-reducer
	     (eris-coap-block-reducer store-url
				      ;; allow up to 10 in-flight puts
				      #:nstart 10)
	     #:block-size 'large
	     #:convergence-secret %null-convergence-secret)))
       (->eris-read-capability read-capability)))

   ;; Get the block size and arity
   (define block-size (eris-read-capability-block-size read-capability))
   (define arity (/ block-size 64))

   ;; How far ahead do we look
   (define lookahead-distance (* block-size arity))

   ;; Set the cache capacity to 2 times the lookahead distance
   (define cache-capacity (* lookahead-distance 2))

   ;; The nstart parameter defines the number of in-flight CoAP
   ;; requests (this is CoAP terminology)
   (define nstart 64)

   ;; Number of lookahead workers
   (define lookahead-workers 48)

   ;; Number of workers
   (define cache-workers (+ lookahead-workers 16)) ; allocate 16 reserve

   ;; Open a CoAP connection
   (define connection (coap.tcp:open-socket-for-uri
		       store-url
		       #:nstart nstart))

   ;; A block-ref function that uses the existing CoAP connection
   (define coap-block-ref
     (lambda (ref) (eris-coap-block-ref store-url ref
					#:connection connection)))

   ;; start a block cache
   (define cache (make-eris-block-cache coap-block-ref
					#:capacity cache-capacity
					#:workers cache-workers))
   (define (cached-block-ref ref)
     (eris-block-cache-block-ref cache ref))

   ;; Decode content
   (eris-transduce

    ;; Plug-in the lookahead transducer
    (teris-lookahead read-capability
		     #:block-ref cached-block-ref
		     #:workers lookahead-workers
		     #:lookahead-distance lookahead-distance)
    ;; (tmap identity) ; use this transducer to disable lookahead

    ;; Define the reducer
    ;; rbytevector ; collect in a bytevector
    rstdout ; write to stdout (useful for pipeing to `pv`)
    ;; rcount ; just count the number of blocks emitted

    read-capability

    ;; use the cache 
    #:block-ref cached-block-ref)))
