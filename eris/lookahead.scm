; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

;; This module implements a lookahead that asynchronously fetches
;; blocks for decoding content before they are really needed.
;;
;; This is important when decoding content from block stores with high
;; latency as otherwise decoding performance would be bound by the store
;; latency.

(define-module (eris lookahead)
  #:use-module (eris decoder)
  #:use-module (eris read-capability)
  #:use-module (eris utils lru-cache)

  #:use-module (fibers)
  #:use-module (fibers operations)
  #:use-module (fibers conditions)
  #:use-module (fibers channels)

  #:use-module (ice-9 match)

  #:export (teris-lookahead))

(define (get-message-or-stop chan stop-condition)
  (perform-operation
   (choice-operation

    ;; get work from the work-channel
    (get-operation chan)

    ;; or stop because stopped? condidtion is set
    (wrap-operation
     (wait-operation stop-condition)
     (lambda () 'stop)))))

(define (worker-loop decoder chan stop-condition content-length)
  (match (get-message-or-stop chan stop-condition)
    (('pos pos)

     (if (< pos content-length)
	 ;; seek to position
	 (worker-loop
	  (eris-decoder-seek decoder pos)
	  chan
	  stop-condition
	  content-length)

	 ;; keep worker alive but don't seek
	 (worker-loop
	  decoder
	  chan
	  stop-condition
	  content-length)))

    ('stop #t)))


(define* (teris-lookahead read-capability
			  #:key block-ref
			  (workers 8)
			  lookahead-distance)
  "Returns a SRFI-171 transducer that can be used with
@var{eris-transduce} to decode content ahead of the main decoder.

The procedure must be called with a running Fibers scheduler.

The read capability must be explictly passed with @var{read-capability}.

The number of lookahead workers can be set with the argument
@var{workers}.  If not provided it will default to 8.

The lookahead will only decode ahead up to @var{lookahead-distance} in
front of the main decoder. The defaut value is @code{(* block-size arity)}."
  (let* ((read-capability (->eris-read-capability read-capability))
	 (block-size (eris-read-capability-block-size read-capability))
	 (arity (/ block-size 64))
	 (decoder (eris-decoder-init read-capability #:block-ref block-ref))
	 (worker-chan (make-channel))
	 (stop-condition (make-condition))
	 (lookahead-distance (or lookahead-distance
	      (* block-size arity 1))))

    (define (lookahead reader-pos lookahead-pos)
      (let loop ((i 0)
		 (reader-pos reader-pos)
		 (lookahead-pos lookahead-pos))

	(if (and
	     ;; still need to look further ahead
	     (< (- lookahead-pos reader-pos) lookahead-distance)
	     ;; enough workers
	     (< i workers))

	    (begin
	      (put-message worker-chan
			   `(pos ,(+ lookahead-pos block-size)))

	      ;; and loop
	      (loop (1+ i) reader-pos (+ lookahead-pos block-size)))

	    ;; return the new lookahead position
	    lookahead-pos)))

    (lambda (reducer)
      (case-lambda

	;; initialization
	(()
	 (let* ((acc (reducer))

		;; initialize the positions
		(reader-pos 0)
		(lookahead-pos 0)

		;; length of content
		(content-length (eris-decoder-length decoder))

		;; seek decoder down to position 0
		(decoder (eris-decoder-seek decoder 0)))

	   ;; spawn workers
	   (for-each
	    (lambda (_)
	      (spawn-fiber
	       (lambda ()
		 (worker-loop decoder
			      worker-chan
			      stop-condition
			      content-length))))
	    (iota workers))

	   ;; The combined accumulator is a cons with position data
	   ;; for lookaheads and the accumulator of @var{reducer}
	   (cons
	    (cons reader-pos
		  ;; instruct lookahead workers
		  (lookahead reader-pos lookahead-pos))
	    ;; the accumulator of @var{reducer}
	    acc)))

	;; accumulation
	((pos-acc value)
	 (match-let* ((((reader-pos . lookahead-pos) . acc) pos-acc)

		      ;; instruct lookahead workers
		      (new-lookahead-pos (lookahead reader-pos lookahead-pos)))

	   ;; cons a combined accumulator
	   (cons
	    (cons (+ reader-pos block-size) new-lookahead-pos)
	    ;; invoke reducer on acc
	    (reducer acc value))))

	;; finalize
	((pos-acc)
	 (match-let* (((_ . acc) pos-acc))

	   ;; stop the workers
	   (signal-condition! stop-condition)

	   ;; finalize reducer
	   (reducer acc)))))))

;; (use-modules (eris)
;; 	     (eris utils buffer)
;; 	     (srfi srfi-71)
;; 	     (srfi srfi-171)
;; 	     (rnrs bytevectors))

;; (run-fibers
;;  (lambda () 
;;    (let* ((read-cap blocks
;; 		    (eris-encode
;; 		     (make-bytevector (* 1024 32))
;; 		     #:block-size 'small
;; 		     #:convergence-secret %null-convergence-secret))
;; 	  (my-block-ref (lambda (ref)
;; 			  (format #t "block-ref\n")
;; 			  (assoc-ref blocks ref)))

;; 	  (decoded (eris-transduce (teris-lookahead read-cap
;; 					    #:block-ref my-block-ref)
;; 			   rbytevector
;; 			   read-cap
;; 			   #:block-ref my-block-ref)))
;;      (bytevector-length decoded))))

