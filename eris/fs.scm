; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris fs)
  #:use-module (eris)
  #:use-module (eris decoder)
  #:use-module (eris read-capability)

  #:use-module (cbor)

  #:use-module (rnrs io ports)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs bytevectors gnu)

  #:use-module (ice-9 ftw)
  #:use-module (ice-9 match)

  #:use-module (srfi srfi-1)   ; List
  #:use-module (srfi srfi-35)  ; Condition
  #:use-module (srfi srfi-43)  ; Vector library
  #:use-module (srfi srfi-171) ; Transducers

  #:export (eris-fs-encode
	    eris-fs-decode
	    eris-fs?))

;; This is an implementation of ERIS-FS
;; (https://eris.codeberg.page/eer/eris-fs.xml). ERIS-FS allows
;; file-systems to be encoded with ERIS while allowing de-duplication of
;; files.

(define (ls-lr path)
  "Returns a sorted list of files under path."
  (define (enter? path stat result) #t)
  (define (leaf name stat result)
    (cons (cons name stat) result))
  (define (down name stat result) result)
  (define (up name stat result) result)
  (define (skip name stat result) result)
  ;; raise errors
  (define (error name stat errno result)
    (raise (condition
	    (&message (message
		       (format #f "~a: ~a~%" (strerror errno) name)))
	    (&error))))

  (sort-list!
   (file-system-fold enter? leaf down up skip error
		     '()
		     path lstat)
   (match-lambda* (((a . _) (b . _)) (string< a b)))))

;; helper to split path into segments
(define (split-path path root)
  (list->vector
   (filter
    (lambda (segment) (< 0 (string-length segment)))
    (string-split
     (string-drop path (string-length root))
     file-name-separator?))))

(define (is-executable? stat)
  (< 0 (logand #o100 (stat:perms stat))))

(define (block-size-recommendation input-size)
  (if (< input-size (* 16 1024))
      1024
      32768))

;;; Encoder

(define* (encode-fso path stat root #:key convergence-secret block-reducer)
  (match (stat:type stat)
    ('regular
     (let* (;; use file size dependant block-size
	    (block-size (block-size-recommendation (stat:size stat)))
	    ;; encode file
	    (read-capability
	     (call-with-input-file path
	       (lambda (port)
		 (eris-encode
		  port
		  #:block-size block-size
		  #:block-reducer block-reducer
		  #:convergence-secret convergence-secret))))
	    ;; type code to indicate regular or executable
	    (type-code (if (is-executable? stat) 1 0)))

       (cons (split-path path root)
	     (vector
	      type-code
	      (make-cbor-tag 276
			     (eris-read-capability->bytevector
			      (->eris-read-capability read-capability)))))))
    ('symlink
     (cons (split-path path root)
	   (vector 2 (readlink path))))

    (_ (raise (condition
	       (&message
		(message
		 (format #f "invalid file type: ~a" path)))
	       (&error))))))

(define* (eris-fs-encode root #:key convergence-secret block-reducer)
  "Encode the file-system tree at @var{path} using ERIS-FS.

Returns two values: The reduce blocks into
@code{block-reducer}. Returns two values: the read capability as URI
string and the reduced blocks of the ERIS-FS index.

Note that the block reducer @var{block-reducer} is used multiple times
for every file encoded. The return values of the block reducer for
individual files is lost."
  (let* (;; sorted list of files
	 (files (ls-lr root))

	 ;; encode files and build index-entries
	 (index-entries (fold
			 (lambda (path-stat index-entries)
			   (match path-stat
			     ((path . stat)
			      (cons
			       (encode-fso path stat root
					   #:convergence-secret convergence-secret
					   #:block-reducer block-reducer)
			       index-entries))))

			 ;; initialize with empty index
			 '()

			 files))

	 ;; encode CBOR index
	 (index (scm->cbor (make-cbor-tag
			    1701996916
			    (vector 1 index-entries)))))
    
    (eris-encode index
		 #:block-size (block-size-recommendation
			       (bytevector-length index))
		 #:convergence-secret convergence-secret
		 #:block-reducer block-reducer)))

;;; Detect ERIS-FS by first five bytes

(define eris-fs-magic
  (u8-list->bytevector (list 218 101 114 105 116)))

(define* (eris-fs? read-capability-or-bv #:key block-ref)
  "Returns @code{#t} if @var{read-capability-or-bv} encodes an ERIS-FS
file system.

When passed a read capability only the first five bytes are decoded
using blocks from @var{block-ref}."
  (if (bytevector? read-capability-or-bv)
      (bytevector=?
       eris-fs-magic
       (bytevector-slice read-capability-or-bv 0 5))

      (let ((decoder (eris-decoder-init
		      (->eris-read-capability read-capability-or-bv)
		      #:block-ref block-ref))
	    (bv (make-bytevector 5)))
	(eris-decoder-read! decoder bv 0 5)
	(eris-fs? bv))))

;;; Decoder

(define* (eris-fs-decode-index read-capability-or-bv #:key block-ref)
  (if (bytevector? read-capability-or-bv)

      (match (cbor->scm read-capability-or-bv)

	(($ <cbor-tag> 1701996916 #(1 index-entries))
	 index-entries)

	(($ <cbor-tag> 1701996916 #(0 _))
	 (error "ERIS-FS version 0 support has been
removed. Sorry. Git has the implementation if you need it."))

	(_ (error "Not a valid ERIS-FS index")))

      ;; attempt to ERIS decode index and re-try
      (eris-fs-decode-index
       (eris-decode read-capability-or-bv #:block-ref block-ref))))

;; Helper to create directories (taken from (guix build utils))
(define (mkdir-p dir)
  "Create directory DIR and all its ancestors."
  (define absolute?
    (string-prefix? "/" dir))

  (define not-slash
    (char-set-complement (char-set #\/)))

  (let loop ((components (string-tokenize dir not-slash))
             (root       (if absolute?
                             ""
                             ".")))
    (match components
      ((head tail ...)
       (let ((path (string-append root "/" head)))
         (catch 'system-error
           (lambda ()
	     ;; create directory and set proper permissions
             (mkdir path)
             (loop tail path))
           (lambda args
             (if (= EEXIST (system-error-errno args))
                 (loop tail path)
                 (apply throw args))))))
      (() #t))))

(define (segments->file-name segments root)
  "Helper to create file names from segment list."
  (string-join
   (cons root
	 (vector->list segments))
   file-name-separator-string))

(define (rfile file)
  "Returns a reducer that outputs to FILE."
  (case-lambda
    (() (open-output-file file #:binary #t))

    ((port bv) (put-bytevector port bv) port)

    ((port) (close-port port))))

(define* (eris-fs-decode read-capability root #:key block-ref)
  "Decode the file-system encoded with @var{read-capability} to @var{root}."
  (for-each
   (match-lambda
     ;; regular
     ((path-segments . #(0 ($ <cbor-tag> 276 file-read-cap)))
      (let ((file (segments->file-name path-segments root)))
	(mkdir-p (dirname file))
	;; decode file
	(eris-transduce (tmap identity)
			(rfile file)
			file-read-cap
			#:block-ref block-ref)
	(utime file 1 1 0 0)
	(chmod file #o444)))

     ;; executable
     ((path-segments . #(1 ($ <cbor-tag> 276 file-read-cap)))
      (let ((file (segments->file-name path-segments root)))
	(mkdir-p (dirname file))
	;; decode file
	(eris-transduce (tmap identity)
			(rfile file)
			file-read-cap
			#:block-ref block-ref)
	(utime file 1 1 0 0)
	(chmod file #o555)))

     ;; symlink
     ((path-segments . #(2 target))
      (let ((file (segments->file-name path-segments root)))
       (mkdir-p (dirname file))
       (symlink target file)
       (utime file 1 1 0 0 AT_SYMLINK_NOFOLLOW)))

     (else
      (raise (condition
	      (&message (message "invalid index entry"))
	      (&error)))))

   (eris-fs-decode-index read-capability #:block-ref block-ref)))

;;; Scratch

;; (use-modules (eris sqlite))

;; (define db (eris-sqlite-open "eris-fs-test.sqlite"))

;; ;; ;; (pk 'index
;; ;; ;;     (eris-fs2-encode
;; ;; ;; "/gnu/store/2w1h8khf84pnwlfg4lf062gzg0svdh0f-ocaml-5.0.0/"
;; ;; ;; 		     #:block-reducer rcount
;; ;; ;; 		     #:convergence-secret %null-convergence-secret))


;; (define read-cap
;;   (eris-fs-encode "/gnu/store/2w1h8khf84pnwlfg4lf062gzg0svdh0f-ocaml-5.0.0/"
;; 		   #:block-reducer (eris-sqlite-block-reducer db)
;; 		   #:convergence-secret %null-convergence-secret))

;; (pk 'eris-fs? (eris-fs? read-cap
			;; #:block-ref (lambda (ref) (eris-sqlite-ref db ref))))

;; (eris-fs-decode read-cap "out" #:block-ref (lambda (ref) (eris-sqlite-ref db ref)))
