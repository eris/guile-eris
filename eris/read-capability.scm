; SPDX-FileCopyrightText: 2020,2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris read-capability)

  #:use-module (eris utils base32)

  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)
  #:use-module (srfi srfi-9)

  #:use-module (web uri)

  #:export (make-eris-read-capability
            eris-read-capability?
            eris-read-capability-block-size
            eris-read-capability-level
            eris-read-capability-root-reference
            eris-read-capability-root-key

            eris-read-capability->bytevector
	    eris-read-capability->string

	    ->eris-read-capability))

(define-record-type <eris-read-capability>
  (make-eris-read-capability block-size level root-reference root-key)
  eris-read-capability?
  (block-size eris-read-capability-block-size)
  (level eris-read-capability-level)
  (root-reference eris-read-capability-root-reference)
  (root-key eris-read-capability-root-key))

(define (eris-read-capability->bytevector read-capability)
  "Return a bytevector containing the binary encoding of the ERIS
read-capability @code{read-capability}."
  (let ((bv (make-bytevector 66)))

    ;; version and block size
    (if (= 1024 (eris-read-capability-block-size read-capability))
	(bytevector-u8-set! bv 0 #x0a)
	(bytevector-u8-set! bv 0 #x0f))

    ;; level
    (bytevector-u8-set! bv 1 (eris-read-capability-level read-capability))

    ;; root reference
    (bytevector-copy!
     (eris-read-capability-root-reference read-capability) 0
     bv 2
     32)

    ;; root key
    (bytevector-copy!
     (eris-read-capability-root-key read-capability) 0
     bv 34
     32)

    ;; return the binary read capability
    bv))

(define (bytevector->eris-read-capability bv)
  (let* ((port (open-bytevector-input-port bv))
         (block-size (get-u8 port))
         (level (get-u8 port))
         (reference (get-bytevector-n port 32))
         (key (get-bytevector-n port 32)))
    (cond

     ((= block-size #x0a)
      (make-eris-read-capability 1024 level reference key))

     ((= block-size #x0f)
      (make-eris-read-capability (* 32 1024) level reference key))

     (else #f))))

(define (eris-read-capability->string read-capability)
  (string-append "urn:eris:"
                 (base32-encode (eris-read-capability->bytevector read-capability))))

(define eris-uri-path-regexp
  (make-regexp "^eris:" regexp/icase))

(define (uri->eris-read-capability uri)
  (case (uri-scheme uri)
    ((urn)
     (if (regexp-exec eris-uri-path-regexp (uri-path uri))
	 (with-exception-handler (const #f)
	   (lambda _
	     (bytevector->eris-read-capability
	      (base32-decode (string-drop (uri-path uri) 5))))
	   #:unwind? #t)
	 #f))
    (else #f)))

(define (string->eris-read-capability urn)
  (let ((uri (string->uri urn)))
    (if uri (uri->eris-read-capability uri)
	#f)))

(define (->eris-read-capability encoded)
  "Attempt to decode the ERIS read capability from string, bytevector or
URI. Returns false if read-capability could not be decoded."
  (cond
   ((eris-read-capability? encoded) encoded)
   ((uri? encoded) (uri->eris-read-capability encoded))
   ((string? encoded) (string->eris-read-capability encoded))
   ((bytevector? encoded) (bytevector->eris-read-capability encoded))
   (else #f)))
