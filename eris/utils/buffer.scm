; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

;; Implements a buffer as a queue of bytevectors. This allows fixed
;; size blocks to be efficiently retrieved from a stream of arbitary
;; sized input bytevectors.

(define-module (eris utils buffer)
  #:use-module (rnrs base) ; for assert
  #:use-module (rnrs bytevectors)

  #:use-module (srfi srfi-9) ; records

  #:use-module (ice-9 q)

  #:export (make-buffer
	    buffer-size
	    buffer-put!
	    buffer-get!

	    rbytevector))

(define-record-type <buffer>
  (&make-buffer queue size front-offset)
  buffer?
  (queue buffer-queue)
  (size buffer-size set-buffer-size!)
  (front-offset buffer-front-offset set-buffer-front-offset!))

;; first piece
(define (buffer-front buf)
  (if (q-empty? (buffer-queue buf))
      (make-bytevector 0)
      (q-front (buffer-queue buf))))

;; first piece remaining bytes
(define (buffer-front-remaining-bytes buf)
  (- (bytevector-length (buffer-front buf))
     (buffer-front-offset buf)))

;; copy len bytes from front to target
(define (buffer-copy-front! buf target target-start len)
  ;; available bytes in first piece is larger or equal to len
  (assert (<= len (buffer-front-remaining-bytes buf)))
  
  ;; copy len bytes to target
  (bytevector-copy!
   (buffer-front buf)
   (buffer-front-offset buf)
   target
   target-start
   len)

  ;; update buffer first piece offset
  (set-buffer-front-offset! buf (+ (buffer-front-offset buf) len))

  ;; update buffer size
  (set-buffer-size! buf (- (buffer-size buf) len))

  ;; if first piece offset is lenght of first piece then pop the first piece
  (if (eqv? (buffer-front-offset buf)
	    (bytevector-length (buffer-front buf)))
      (begin
	(deq! (buffer-queue buf))
	(set-buffer-front-offset! buf 0))))

(define (make-buffer)
  (&make-buffer (make-q) 0 0))

(define (buffer-put! buf bv)
  (set-buffer-size! buf (+
			 (buffer-size buf)
			 (bytevector-length bv)))

  (enq! (buffer-queue buf) bv))

(define (buffer-get! buf n)
  ;; buffer must be larger than n
  (assert (<= n (buffer-size buf)))
  (let ((target (make-bytevector n)))

    (let loop ((r n))

      ;; front remaining bytes
      (let ((frb (buffer-front-remaining-bytes buf)))

	(cond
	 ;; nothing left to read, return target
	 ((eqv? r 0) target)

	 ;; remaining bytes can be copied from front
	 ((<= r frb)
	  (begin
	    (buffer-copy-front! buf target (- n r) r)
	    target))

	 ;; copy all remaining bytes and loop
	 (else
	  (buffer-copy-front! buf target (- n r) frb)
	  (loop (- r frb))))))))

;; (let* ((buf (make-buffer)))
;;   (buffer-put! buf (make-bytevector 10))
;;   (buffer-get! buf 1)
;;   buf)

(define rbytevector
  (case-lambda
    (() (make-buffer))
    ((buffer) (buffer-get! buffer (buffer-size buffer)))
    ((buffer bv)
     (buffer-put! buffer bv)
     buffer)))
