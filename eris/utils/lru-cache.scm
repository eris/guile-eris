; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris utils lru-cache)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-69)

  #:export (make-lru-cache
	    lru-cache?
	    lru-cache-weight
	    lru-cache-capacity
	    lru-cache-add!
	    lru-cache-find))

;; This module implements a LRU cache backed by a doubly-linked-list
;; and a hash-table. The implementation is not thread-safe! And it should
;; probably moved to another library and be better tested...

(define-record-type <node>
  (make-node key value weight prev next)
  node?
  (key node-key)
  (value node-value set-node-value!)
  (weight node-weight)
  (prev node-prev set-node-prev!)
  (next node-next set-node-next!))

(define-record-type <lru-cache>
  (&make-lru-cache ht first last weight capacity)
  lru-cache?
  (ht ht)
  (first first set-first!)
  (last last set-last!)
  (weight lru-cache-weight set-weight!)
  (capacity lru-cache-capacity))

(define (lru-cache-empty? cache)
  (not (first cache)))

(define* (make-lru-cache
	  #:key
	  capacity
	  (hash-table (make-hash-table)))
  (&make-lru-cache
   hash-table
   #f #f 0 capacity))

(define (dll-append! cache node)
  "Append @var{node} to the doubly-linked-list of the cache."
  (if (lru-cache-empty? cache)
      (begin
	(set-first! cache node)
	(set-last! cache node))
      (begin
	(set-node-next! (last cache) node)
	(set-node-prev! node (last cache))
	(set-last! cache node))))

(define (dll-detatch! cache node)
  (cond ((and (node-prev node)
	      (node-next node))
	 ;; node has prev and next (is not last or first)

	 (set-node-next!
	  (node-prev node)
	  (node-next node))

	 (set-node-prev!
	  (node-next node)
	  (node-prev node))

	 (set-node-next! node #f)
	 (set-node-prev! node #f))

	((node-prev node)
	 ;; node is last (but not first)

	 (set-node-next! (node-prev node) #f)
	 (set-last! cache (node-prev node))
	 (set-node-prev! node #f))

	((node-next node)
	 ;; node is first (but not last)

	 (set-node-prev! (node-next node) #f)
	 (set-first! cache (node-next node))
	 (set-node-next! node #f))

	(else
	 ;; node is first and last
	 (set-first! cache #f)
	 (set-last! cache #f))))

(define (dll->list cache)
  (unfold
   not
   (lambda (node)
     (cons (node-value node)
	   (node-weight node)))
   node-next
   (first cache)))

(define* (lru-cache-trim! cache)
  (if (< (lru-cache-capacity cache)
	 (lru-cache-weight cache))

      (let ((first (first cache)))
	(dll-detatch! cache first)
	(hash-table-delete! (ht cache) (node-key first))
	(set-weight! cache (- (lru-cache-weight cache)
			      (node-weight first)))
	;; recursively trim
	(lru-cache-trim! cache))

      (lru-cache-weight cache)))

(define (lru-cache-promote! cache node)
  (dll-detatch! cache node)
  (dll-append! cache node))

(define* (lru-cache-add! cache key value #:key (weight 1))
  (hash-table-update! (ht cache) key
		      (lambda (node)
			(lru-cache-promote! cache node)
			(set-node-value! node value)
			node)
		      (lambda ()
			(let ((node (make-node key value weight #f #f)))
			  (dll-append! cache node)
			  (set-weight! cache (+ (lru-cache-weight cache) weight))
			  (lru-cache-trim! cache)
			  node))))

(define (lru-cache-find cache key)
  (let ((node (hash-table-ref (ht cache) key (const #f))))

    (if (node? node)

	;; detatch node and add to end of doubly-linked-list
	(begin
	  (lru-cache-promote! cache node)
	  (node-value node))

	#f)))

;; (define my-lru-cache
;;   (make-lru-cache #:capacity 2))

;; (lru-cache-add! my-lru-cache 1 "hello")
;; (dll->list my-lru-cache)
;; (lru-cache-add! my-lru-cache 2 "hi")
;; (dll->list my-lru-cache)
;; (lru-cache-add! my-lru-cache 3 "grüezi" #:weight 2)
;; (dll->list my-lru-cache)
;; (lru-cache-add! my-lru-cache 4 "hoi")
;; (dll->list my-lru-cache)
;; (lru-cache-add! my-lru-cache 5 "salut")
;; (dll->list my-lru-cache)

;; (first my-lru-cache)

;; (hash-table->alist (ht my-lru-cache))

;; (lru-cache-weight my-lru-cache)

;; (lru-cache-find my-lru-cache 2)
;; (lru-cache-find my-lru-cache 1)
;; (lru-cache-find my-lru-cache 3)

