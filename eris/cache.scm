; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris cache)
  #:use-module (eris utils lru-cache)

  #:use-module (fibers)
  #:use-module (fibers operations)
  #:use-module (fibers channels)
  #:use-module (fibers conditions)

  #:use-module (srfi srfi-9)  ; Records
  #:use-module (srfi srfi-69) ; Basic hash tables

  #:use-module (rnrs bytevectors)

  #:use-module (ice-9 match)

  #:export (<eris-block-cache>
	    make-eris-block-cache
	    eris-block-cache?
	    eris-block-cache-block-ref
	    eris-block-cache-block-ref-operation
	    eris-block-cache-stop))


(define-record-type <eris-block-cache>
  (%make-eris-block-cache block-ref
			  pending
			  cache
			  control-channel
			  work-channel
			  stopped?)
  eris-block-cache?
  (block-ref get-block-ref)
  (pending get-pending)
  (cache get-cache)
  (control-channel get-control-channel)
  (work-channel get-work-channel)
  (stopped? get-stopped?))

(define (control-loop cache)
  (let ((pending (get-pending cache))
	(lru (get-cache cache)))
    (match (get-message (get-control-channel cache))

      ;; Handle a block-ref request
      (('block-ref ref reply)

       (cond
	;; Block is in LRU cache
	((lru-cache-find lru ref) =>
	 (lambda (block) (put-message reply block)))

	;; Request for the block is already pending
	((hash-table-exists? pending ref)

	 ;; add reply to list of consumers that should be notified
	 (hash-table-update!
	  pending ref
	  (lambda (replies) (cons reply replies))))

	;; Start a new request
	(else

	 ;; add reply to pending table
	 (hash-table-set! pending ref (list reply))

	 ;; send request to worker
	 (put-message (get-work-channel cache)
		      `(get ,ref))))

       ;; loop
       (control-loop cache))

      ;; Handle a block retrieved by a worker
      (('block ref block)

       ;; notify consumers
       (for-each
	(lambda (reply) (put-message reply block))
	(hash-table-ref/default pending ref '()))

       ;; clear ref from pending table
       (hash-table-delete! pending ref)

       ;; Add to LRU 
       (when block (lru-cache-add! lru ref block
				   #:weight (bytevector-length block)))

       ;; loop
       (control-loop cache))

      ;; Stop the cache
      ('stop (signal-condition! (get-stopped? cache))))))


(define (get-worker-message cache)
  (let ((op (choice-operation

	     ;; get work from the work-channel
	     (get-operation (get-work-channel cache))

	     ;; or stop because stopped? condidtion is set
	     (wrap-operation
	      (wait-operation (get-stopped? cache))
	      (lambda () 'stop)))))

    (perform-operation op)))

(define (worker-loop cache)
  (match (get-worker-message cache)

    ;; Get a block
    (('get ref)

     (let* ((block-ref (get-block-ref cache))
	    (block (block-ref ref)))

       ;; send retrieved block to controller
       (put-message (get-control-channel cache)
		    `(block ,ref ,block))


       ;; and loop
       (worker-loop cache)))

    ;; Stop
    ('stop #t)))

(define* (make-eris-block-cache block-ref #:key
				;; default capacity is 512 large blocks
				(capacity (* 32 1024 512))
				(workers 8))
  "Create an ERIS block cache using the underlying block reference
function @var{block-ref}.

The procedure must be called with a running fibers scheduler.

The capacity of the cache in bytes can be set with the argument
@var{capacity}. The default capacity is 16MiB (512 blocks of size
32KiB).

Block requests are handled by a pool of concurrent workers. The number
of workers can be set with the argument @var{workers} (defaults to 8)."
  (define cache (%make-eris-block-cache
		 block-ref
		 ;; a table of pending requests (from block ref to
		 ;; list of reply channels)
		 (make-hash-table bytevector=? hash)
		 ;; create LRU cache
		 (make-lru-cache #:capacity capacity)
		 ;; control channel
		 (make-channel)
		 ;; work channel
		 (make-channel)
		 ;; stopped condition
		 (make-condition)))

  ;; spawn workers
  (for-each
   (lambda (_) (spawn-fiber (lambda () (worker-loop cache))))
   (iota workers))

  ;; spawn the controller
  (spawn-fiber (lambda () (control-loop cache)))

  ;; return the cache
  cache)

(define (eris-block-cache-stop cache)
  "Stop the block cache"
  (put-message (get-control-channel cache) 'stop))

(define (eris-block-cache-block-ref-operation cache ref)
  "Return an operation that when performed would request a block wit
ref @var{ref} from the cache and if not available in cache from the
underlying block reference function."
  ;; createa channel to carry the reply
  (define reply (make-channel))

  ;; return a wrapped operation
  (wrap-operation
   (put-operation (get-control-channel cache)
		  `(block-ref ,ref ,reply))
   (lambda () (get-message reply))))

(define (eris-block-cache-block-ref cache ref)
  "Try and get a block with reference @var{ref} from the block cache
@var{cache}.  If block is not cached the block will be fetched using
the block reference function of the cache."
  (perform-operation (eris-block-cache-block-ref-operation cache ref)))

