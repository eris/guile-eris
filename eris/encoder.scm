; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later


(define-module (eris encoder)
  #:use-module (rnrs base)
  #:use-module (rnrs io ports)
  #:use-module (rnrs bytevectors)

  #:use-module (eris read-capability)
  #:use-module (eris utils buffer)

  #:use-module (sodium generichash)
  #:use-module (sodium padding)
  #:use-module (sodium stream)

  #:use-module (srfi srfi-9) ; records
  #:use-module (srfi srfi-34) ; exceptions handling
  #:use-module (srfi srfi-35) ; conditions
  #:use-module (srfi srfi-43) ; vector library
  #:use-module (srfi srfi-71) ; extended let-syntax for multiple values
  #:use-module (srfi srfi-171) ; transducers

  #:export (<eris-encoder>
	    eris-encoder-init
	    eris-encoder?
	    eris-encoder-update
	    eris-encoder-finalize

	    eris-encoder-result
	    set-eris-encoder-result!))

;; Encoder State

(define-record-type <eris-encoder>
  (&make-eris-encoder
   ;; encoding parameters
   convergence-secret
   block-size
   arity ; for convenience also store the arity

   ;; block reducer
   block-reducer
   result

   ;; input buffer
   buffer

   ;; levels is a vector of reference-key pairs (as list)
   levels)

  eris-encoder?

  (convergence-secret encoder-convergence-secret)
  (block-size encoder-block-size)
  (arity encoder-arity)
  (block-reducer encoder-block-reducer)
  (result eris-encoder-result set-eris-encoder-result!)
  (buffer encoder-buffer)
  (levels encoder-levels))

(define* (eris-encoder-init
	  #:key
	  block-size
	  (block-reducer reverse-rcons)
	  convergence-secret
	  (identity (block-reducer)))

  ;; ensure that convergence secret is provided
  (assert (and (bytevector? convergence-secret)
	       (eqv? 32 (bytevector-length convergence-secret))))

  (&make-eris-encoder
   convergence-secret
   block-size
   (/ block-size 64)
   block-reducer
   identity
   (make-buffer)
   (make-vector 256 #f)))

(define (block-put encoder reference block)
  (let* ((reducer (encoder-block-reducer encoder))
	 (blocks (eris-encoder-result encoder)))
    (set-eris-encoder-result! encoder (reducer blocks (cons reference block)))))

(define (get-level encoder k)
  "Returns all the reference-key pairs at level. Note that the order
is reversed."
  (case (vector-ref (encoder-levels encoder) k)
    ((#f) '())
    (else => identity)))

(define (top-level encoder)
  "Returns the count of the top level of the tree."
  (1- (vector-index not (encoder-levels encoder))))

(define (add-reference-key-pair-to-level! encoder k reference-key)
  "Add a reference-key pair to level."
  (vector-set! (encoder-levels encoder) k
	       (cons reference-key (get-level encoder k))))

(define (clear-level! encoder k)
  "Clear all reference-key pairs in level."
  (vector-set! (encoder-levels encoder) k '()))

;; Cryptographic Primitives

(define (blake2b-256 bv)
  (crypto-generichash bv #:out-len 32))

(define* (blake2b-256-keyed bv #:key key)
  (crypto-generichash bv #:out-len 32 #:key key))

(define* (chacha20 bv #:key key nonce)
  (crypto-stream-chacha20-ietf-xor #:message bv
				   #:nonce nonce
				   #:key key))

;; Encrypting blocks

(define (level-nonce level)
  "Returns the nonce that is used for encrypting/decrypting nodes at given level."
  (u8-list->bytevector (cons level (make-list 11 0))))

(define* (encrypt-leaf-node node #:key convergence-secret)
  "Encrypt leaf node and return the block, reference and key."
  (let* (;; use the keyed Blake2b hash to compute the encryption key
	 (key (blake2b-256-keyed node #:key convergence-secret))
	 ;; encrypt node to block
	 (block (chacha20 node #:key key #:nonce (level-nonce 0)))
	 ;; compute reference to encrypted block using unkeyed Blake2b
	 (ref (blake2b-256 block)))
    (values block ref key)))

(define* (encrypt-internal-node node #:key level)
  "Encrypt node and return the block, reference and key."
  (let* (;; use the unkeyed Blake2b hash to compute the encryption key
	 (key (blake2b-256 node))
	 ;; encrypt node to block
	 (block (chacha20 node #:key key #:nonce (level-nonce level)))
	 ;; compute reference to encrypted block using unkeyed Blake2b
	 (ref (blake2b-256 block)))
    (values block ref key)))

;; Encoding Algorithm

(define* (construct-internal-node reference-key-pairs #:key arity)
  "Returns internal node holding given reference-key pairs."

  ;; Note that this is different than the Construct-Internal-Nodes
  ;; procedure as given in the ERIS specification. Here we know that the
  ;; number of reference-key-pairs is always less than or equal to arity
  ;; (as we eagerly collect reference-key pairs to nodes)

  ;; (assert (<= (length reference-key-pairs) arity))

  (let* ((port get-bytevector (open-bytevector-output-port)))
    ;; add reference key pairs to node
    (map
     (lambda (reference-key)
       ;; put the reference
       (put-bytevector port (car reference-key))
       ;; put the key
       (put-bytevector port (cdr reference-key)))
     reference-key-pairs)

    ;; add padding
    (put-bytevector port
		    (make-bytevector
		     (* 64 (- arity (length reference-key-pairs)))
		     0))

    ;; get the bytevector (node)
    (get-bytevector)))

(define* (force-collect encoder k)
  "Force the combination of all rk pairs at level to a node at level above"
  (let* ((block reference key (encrypt-internal-node
			       (construct-internal-node
				;; get the ref-key pairs for
				;; level. Order needs to be reversed.
				(reverse (get-level encoder k))
				#:arity (encoder-arity encoder))
			       ;; level of this node will be one above
			       ;; the level we are collecting
			       #:level (1+ k))))

    ;; clear the level
    (clear-level! encoder k)

    ;; add the ref-key pair to the next level
    (add-reference-key-pair-to-level! encoder (1+ k)
				      (cons reference key))

    ;; return the block-reference-key triple
    (values block reference)))

(define* (collect encoder k)
  "Recursively collect all ref-key pairs at a level k to a node at the
level above if the arity is reached. Returns a list of reference-block
pairs of newly created blocks."
  (let ((arity (encoder-arity encoder)))

    (if (<= arity (length (get-level encoder k)))
	;; level is full, collect all reference-key pairs to a node in the level above
	(let* ((block reference (force-collect encoder k)))

	  ;; store the block
	  (block-put encoder reference block)

	  ;; recursively collect at the next level
          (collect encoder (1+ k))))))

(define (eris-encoder-update encoder bv)

  (let ((buf (encoder-buffer encoder))
	(block-size (encoder-block-size encoder))
	(convergence-secret (encoder-convergence-secret encoder)))

    ;; add bv to input buffer
    (buffer-put! buf bv)


    (while (<= block-size (buffer-size buf))

      (let* ((leaf-node (buffer-get! buf block-size))
	     (block reference key
		    (encrypt-leaf-node
		     leaf-node
		     #:convergence-secret convergence-secret)))

	;; store block
	(block-put encoder reference block)

	;; add reference to encrypted data block to level 0 of the tree
	(add-reference-key-pair-to-level! encoder 0 (cons reference key))

	;; collect full nodes starting at level 0
	(collect encoder 0)))
    encoder))

(define* (finalize encoder k)
  "Recursively collects all ref-key pairs to a single root reference"
  (cond
   ;; current level is top level and has exactly one node - our root reference
   ((and (= k (top-level encoder))
         (= (length (get-level encoder k)) 1))
    (let ((root-ref-key (car (get-level encoder k))))
      (make-eris-read-capability
       (encoder-block-size encoder)
       k
       (car root-ref-key)
       (cdr root-ref-key))))

   ;; level is not empty
   ((< 0 (length (get-level encoder k)))
    ;; collect all ref-key pairs at level
    (let* ((block reference (force-collect encoder k)))

      ;; emit the block
      (block-put encoder reference block)

      ;; continue finalizing at level above
      (finalize encoder (1+ k))))
   
   ;; level is empty
   ((null? (get-level encoder k))
    ;; continue finalizing at level above
    (finalize encoder (1+ k)))))

(define* (eris-encoder-finalize encoder #:key (finalize-reducer? #t))

  (let* ((buf (encoder-buffer encoder))

	 ;; get remainder bytes from buffer and pad
	 (remainder (buffer-get! buf (buffer-size buf)))
	 (block-size (encoder-block-size encoder))
	 (padded (sodium-pad remainder #:block-size block-size))

	 ;; update encoder with padded remainder
	 (_ (eris-encoder-update encoder padded))

	 ;; finalize tree to read capability
	 (read-capability (finalize encoder 0))

	 ;; finalize the block-reducer
	 (block-reducer (encoder-block-reducer encoder))
	 (result (if finalize-reducer?
		     (block-reducer (eris-encoder-result encoder))
		     (eris-encoder-result encoder))))

    (values read-capability result)))


;; (let* ((encoder (eris-encoder-init 1024 #:block-reducer rcount))
;;        (_ (eris-encoder-update encoder (make-bytevector 1024)))
;;        (read-capability blocks (eris-encoder-finalize encoder)))
;;   blocks)

