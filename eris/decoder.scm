; SPDX-FileCopyrightText: 2020-2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris decoder)
  #:use-module (eris read-capability)

  #:use-module (sodium generichash)
  #:use-module (sodium stream)
  #:use-module (sodium padding)

  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:use-module (ice-9 match)

  #:use-module (srfi srfi-9) ; records
  #:use-module (srfi srfi-34) ; exceptions handling
  #:use-module (srfi srfi-35) ; conditions
  #:use-module (srfi srfi-71) ; extended let-syntax for multiple values
  #:use-module (srfi srfi-171) ; transducers

  #:export (eris-decoder-init
	    <eris-decoder>
	    eris-decoder?
	    eris-decoder-position
	    eris-decoder-seek
	    eris-decoder-length
	    eris-decoder-read!))

;; Error handling

(define &decode-error
  (make-condition-type 'decode-error &error '()))

(define &block-ref-error
  (make-condition-type 'block-ref-error &decode-error '(ref)))

(define (raise-block-ref-error ref msg)
  (raise
   (condition
    (&block-ref-error (ref ref))
    (&message (message msg)))))

(define &invalid-internal-node-error
  (make-condition-type 'invalid-internal-node &decode-error '()))

;; Reference-Key Pair

(define-record-type <reference-key-pair>
  (make-reference-key-pair ref key)
  reference-key-pair?
  (ref reference-key-pair-ref)
  (key reference-key-pair-key))

;; Leaf Node

(define-record-type <leaf-node>
  (make-leaf-node content offset)
  leaf-node?
  (content leaf-node-content)
  (offset leaf-node-offset))

(define (leaf-node-remaining-bytes leaf-node)
  (- (bytevector-length (leaf-node-content leaf-node))
     (leaf-node-offset leaf-node)))

(define (leaf-node-eof? leaf-node)
  (eqv? (leaf-node-remaining-bytes leaf-node) 0))

;; Decoder State

(define-record-type <eris-decoder>
  (make-eris-decoder
   read-capability
   block-ref
   block-size

   ;; Zipper
   left
   cur
   up
   right

   ;; Additional location information
   level
   level-count
   is-last?)
  eris-decoder?

  (read-capability decoder-read-capability)
  (block-ref decoder-block-ref)
  (block-size decoder-block-size)

  (left decoder-left)
  (cur decoder-cur)
  (up decoder-up)
  (right decoder-right)

  (level decoder-level)
  (level-count decoder-level-count)
  (is-last? decoder-is-last?))

(define (decoder-read-capability-level decoder)
  (eris-read-capability-level (decoder-read-capability decoder)))

(define (block-ref decoder ref)
  ((decoder-block-ref decoder) ref))

(define (is-cur-rk-pair? decoder)
  (reference-key-pair? (decoder-cur decoder)))

(define (is-cur-leaf-node? decoder)
  (leaf-node? (decoder-cur decoder)))

(define (rk-width decoder)
  "Returns the width (in bytes) of content that can be reached by a
single reference-key pair at the current level."
  (* (expt
      (/ (decoder-block-size decoder) 64) ; arity
      (- (decoder-level decoder) 1))
     (decoder-block-size decoder)))

(define (decoder-eof? decoder)
  (and (is-cur-leaf-node? decoder)
       (leaf-node-eof? (decoder-cur decoder))))

(define* (eris-decoder-init read-capability #:key block-ref)
  (make-eris-decoder
   read-capability
   block-ref
   (eris-read-capability-block-size read-capability)

   '() ; left
   ;; cur
   (make-reference-key-pair
    (eris-read-capability-root-reference read-capability)
    (eris-read-capability-root-key read-capability))
   #f ; up
   '() ; right

   ;; level is one above the level encoded in read-capability
   (+ (eris-read-capability-level read-capability) 1)
   0 ; level-count
   #t ; is-last?
   ))

(define (eris-decoder-position decoder)
  "Returns the current position of the decoder."
  (match decoder
    (($ <eris-decoder>
	read-capability block-ref block-size
	left ($ <reference-key-pair> _) up right
	level level-count is-last?)
     (* level-count (rk-width decoder)))

    (($ <eris-decoder>
	read-capability block-ref block-size
	left ($ <leaf-node> content offset) up right
	level level-count is-last?)
     (+ (* block-size level-count) offset))))

;; Range

(define-record-type <range>
  (make-range lower upper)
  range?
  (lower range-lower)
  (upper range-upper))

(define (range-within range pos)
  (and (<= (range-lower range) pos)
       (<= pos (range-upper range))))

(define (range-above range pos)
  (< (range-upper range) pos))

(define (range-below range pos)
  (< pos (range-lower range)))

(define (range-cur decoder)
  "Returns the range of content that can be reached from the current location."
  (match decoder
    (($ <eris-decoder>
	read-capability block-ref block-size
	left ($ <reference-key-pair> _) up right
	level level-count is-last?)

     (make-range
      (eris-decoder-position decoder)
      (1- (* (+ level-count 1) (rk-width decoder)))))

    (($ <eris-decoder>
	read-capability block-ref block-size
	left ($ <leaf-node> content offset) up right
	level level-count is-last?)

     (make-range
      (* block-size level-count)
      (1- (+ (* block-size level-count)
	     (bytevector-length content)))))))

(define (range-node decoder)
  "Returns the range of content that can be reached from the current
node (location + siblings)."
  (match decoder
    (($ <eris-decoder>
	read-capability block-ref block-size
	left ($ <reference-key-pair> _) up right
	level level-count is-last?)

     (make-range
      (* (- level-count
	    (length left))
	 (rk-width decoder))
      (- (* (+ level-count
	       (length right)
	       1)
	    (rk-width decoder))
	 1)))

    (($ <eris-decoder>
	read-capability block-ref block-size
	left ($ <leaf-node> content offset) up right
	level level-count is-last?)

     (make-range
      (* block-size level-count)
      (+ (* block-size level-count)
	 (bytevector-length content))))))


;; Movement

(define (move-right decoder)
  (match decoder
    (($ <eris-decoder>
	read-capability block-ref block-size
	left cur up (new-cur . right)
	level level-count is-last?)

     (make-eris-decoder
      read-capability block-ref block-size

      (cons cur left)
      new-cur
      up
      right

      level
      (+ level-count 1)
      (and (decoder-is-last? up)
	   (null? right))))

    (_ #f)))

(define (move-left decoder)
  (match decoder
    (($ <eris-decoder>
	read-capability block-ref block-size
	(new-cur . left) cur up right
	level level-count is-last?)

     (make-eris-decoder
      read-capability block-ref block-size

      left
      new-cur
      up
      (cons cur right)

      level
      (- level-count 1)
      #f))

    (_ #f)))

(define (move-up decoder)
  (if (eris-decoder? (decoder-up decoder))
      (decoder-up decoder)
      #f))

(define (move-down decoder)


  (define (level-nonce level)
    "Returns the nonce that is used for encrypting/decrypting nodes at given level."
    (u8-list->bytevector (cons level (make-list 11 0))))

  (define* (get-block ref-key #:key level)
    "Get and unencrypte a block"
    (let* ((ref (reference-key-pair-ref ref-key))
	   (key (reference-key-pair-key ref-key))
	   (block (block-ref decoder ref)))

      (unless (bytevector? block)
	(raise-block-ref-error ref "can not dereference block"))

      (unless (equal? (bytevector-length block) (decoder-block-size decoder))
	(raise-block-ref-error ref "block has invalid size"))

      (unless (bytevector=? ref
			    (crypto-generichash block #:out-len 32))
	(raise-block-ref-error ref "retrieved block does not match its reference"))

      (crypto-stream-chacha20-ietf-xor #:message block
				       #:nonce (level-nonce level)
				       #:key key)))

  (define* (get-leaf-node ref-key #:key is-last?)
    (let ((block (get-block ref-key #:level 0)))
      (make-leaf-node
       (if is-last?
	   (sodium-unpad block #:block-size (decoder-block-size decoder))
	   block)
       0)))

  (define null-ref
    (make-bytevector 32 0))

  (define (null-ref? bv)
    (bytevector=? bv null-ref))

  (define (bytevector-is-zero-or-eof? bv)
    (or (eof-object? bv)
	(bytevector-u8-transduce
	 (tmap identity)
	 (revery (lambda (byte) (equal? byte #x00))) bv)))

  (define* (get-internal-node ref-key #:key level)
    "Retrieve a block and decodes it as a list of reference-key pairs"
    (let* ((block (get-block ref-key #:level level)))

      (when (and (< 0 level) (eqv? level (decoder-read-capability-level decoder)))
	(unless (bytevector=? (crypto-generichash block #:out-len 32)
			      (reference-key-pair-key ref-key))
	  (raise (condition
		  (&message
		   (message "invalid key in read capability"))
		  (&error)))))

      (port-transduce
       (tmap identity)
       rcons
       (lambda (port)
	 ;; read reference and key from block
	 (let ((ref (get-bytevector-n port 32))
	       (key (get-bytevector-n port 32)))
	   (cond
	    ;; if both object and key are eof we have reached end of node, return eof
	    ((and (eof-object? ref) (eof-object? key)) (eof-object))
	    ;; if null-ref make sure the rest of node is null and return eof
	    ((null-ref? ref) (if (bytevector-is-zero-or-eof? (get-bytevector-all port))
				 (eof-object)
				 ;; if not the internal node is invalid
				 (raise (make-condition &invalid-internal-node-error))))
	    ;; else return reference key pair
	    (else (make-reference-key-pair ref key)))))
       (open-bytevector-input-port block))))

  (match decoder
    (($ <eris-decoder>
	read-capability block-ref block-size
	left cur up right
	level level-count is-last?)

     (if (reference-key-pair? cur)
	 (if (eqv? level 1)

	     (make-eris-decoder
	      read-capability block-ref block-size

	      '()
	      (get-leaf-node cur #:is-last? is-last?)
	      decoder		; set current decoder as up
	      '()

	      0
	      level-count
	      is-last?)

	     ;; get internal node
	     (let* ((ref-key-pairs (get-internal-node cur #:level (- level 1)))
		    (new-cur (car ref-key-pairs))
		    (right (cdr ref-key-pairs)))

	       (make-eris-decoder
		read-capability block-ref block-size

		'() ;left
		new-cur
		decoder ; set current decoder as up
		right

		(- level 1)
		(* level-count (/ block-size 64))
		(and is-last? (null? right)))))

	 ;; cur is not a reference-key pair
	 #f))

    (_ #f)))


;; Seek

(define (eris-decoder-seek-eof decoder)
  (cond

   ((and (decoder-is-last? decoder)
	 (is-cur-leaf-node? decoder))
    (make-eris-decoder
     (decoder-read-capability decoder)
     (decoder-block-ref decoder)
     (decoder-block-size decoder)

     (decoder-left decoder)
     (make-leaf-node
      (leaf-node-content (decoder-cur decoder))
      (bytevector-length (leaf-node-content (decoder-cur decoder))))
     (decoder-up decoder)
     (decoder-right decoder)

     (decoder-level decoder)
     (decoder-level-count decoder)
     (decoder-is-last? decoder)))


   ((and (decoder-is-last? decoder)
	 (is-cur-rk-pair? decoder))
    ;; we are at last rk-pair, move down
    (eris-decoder-seek-eof (move-down decoder)))

   ((and (decoder-up decoder)
	 (decoder-is-last? (decoder-up decoder)))
    ;; parent is last. Move to right.
    (eris-decoder-seek-eof (move-right decoder)))

   (else
    ;; else move up
    (eris-decoder-seek-eof (move-up decoder)))))

(define (eris-decoder-seek decoder pos)

  (let ((range-cur (range-cur decoder))
	(range-node (range-node decoder)))

    ;; (format #t "eris-decoder-seek: cur=~a pos:~a range-cur:~a range-node:~a\n"
    ;; 	    (decoder-cur decoder)
    ;; 	    pos
    ;; 	    range-cur
    ;; 	    range-node)

    (cond
     ((and (range-within range-cur pos)
	   (is-cur-rk-pair? decoder))
      ;; move down and continue seek
      ;; (format #t "move-down\n")
      (eris-decoder-seek (move-down decoder) pos))

     ((and (range-within range-cur pos)
	   (is-cur-leaf-node? decoder))

      ;; (format #t "at leaf\n")

      (make-eris-decoder
       (decoder-read-capability decoder)
       (decoder-block-ref decoder)
       (decoder-block-size decoder)

       (decoder-left decoder)
       (make-leaf-node
	(leaf-node-content (decoder-cur decoder))
	(- pos (* (decoder-level-count decoder)
		  (decoder-block-size decoder))))
       (decoder-up decoder)
       (decoder-right decoder)

       (decoder-level decoder)
       (decoder-level-count decoder)
       (decoder-is-last? decoder)))

     ((and (is-cur-leaf-node? decoder)
	   (decoder-is-last? decoder)
	   (range-above range-cur pos))
      ;; pos is above range of last leaf node. Set leaf-node-offset just beyond content.
      (make-eris-decoder
       (decoder-read-capability decoder)
       (decoder-block-ref decoder)
       (decoder-block-size decoder)

       (decoder-left decoder)
       (make-leaf-node
	(leaf-node-content (decoder-cur decoder))
	(bytevector-length (leaf-node-content (decoder-cur decoder))))
       (decoder-up decoder)
       (decoder-right decoder)

       (decoder-level decoder)
       (decoder-level-count decoder)
       (decoder-is-last? decoder)))

     ((and (range-within range-node pos)
	   (range-above range-cur pos)
	   (is-cur-rk-pair? decoder))
      ;; (format #t "move-right\n")
      (eris-decoder-seek (move-right decoder) pos))

     ((and (range-within range-node pos)
	   (range-below range-cur pos)
	   (is-cur-rk-pair? decoder))
      ;; (format #t "move-left\n")
      (eris-decoder-seek (move-left decoder) pos))

     (else
      ;; position is not in range
      (if (decoder-up decoder)
	  ;; move up if possible
	  (begin
	    ;; (format #t "move-up\n")
	    (eris-decoder-seek (move-up decoder) pos))
	  ;; position is out of encoded content range, go to eof
	  (eris-decoder-seek-eof decoder))))))

(define (eris-decoder-length decoder)
  (eris-decoder-position (eris-decoder-seek-eof decoder)))

(define (eris-decoder-read! decoder target target-start len)
  ;; ensure that we are at leaf node by seeking to current position
  (let ((decoder (eris-decoder-seek decoder (eris-decoder-position decoder))))

    (let loop ((decoder decoder) (i 0))

      (cond

       ;; nothing left to read return count of read bytes and new decoder
       ((eqv? len i) (values i decoder))

       ((decoder-eof? decoder) (values i decoder))

       ;; read from current leaf-node
       ((eris-decoder? decoder) (let* ((leaf-node (decoder-cur decoder))
				       (remaining (leaf-node-remaining-bytes leaf-node))
				       (c (min remaining (- len i))))

				  (when (< 0 c)
				    ;; copy c bytes from leaf node
				    (bytevector-copy!
				     (leaf-node-content leaf-node)
				     (leaf-node-offset leaf-node)
				     target
				     (+ target-start i)
				     c))

				  (loop
				   ;; seek to next leaf-node
				   (eris-decoder-seek
				    decoder
				    (+ (eris-decoder-position decoder) c))
				   ;; increment amount already read
				   (+ i c))))))))


;; (use-modules (eris encoder))

;; (let* ((encoder (eris-encoder-init 32768))
;;        (_ (eris-encoder-update encoder
;; 			       (make-bytevector 12123012)
;; 			       ;; (string->utf8 "1234")
;; 			       ))
;;        (read-capability blocks (eris-encoder-finalize encoder))

;;        (target (make-bytevector 32))

;;        (decoder (eris-decoder-init read-capability
;; 				   #:block-ref (lambda (ref) (assoc-ref blocks ref)))))

;;   (eris-decoder-length (eris-decoder-seek-eof decoder)))


