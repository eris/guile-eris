; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris coap)

  #:use-module (coap message)
  #:use-module (coap code)
  #:use-module (coap option)
  #:use-module ((coap tcp) #:prefix coap.tcp:)
  #:use-module (web uri)

  #:use-module (fibers)
  #:use-module (fibers channels)

  #:use-module (rnrs bytevectors)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)  ; and-let*
  #:use-module (srfi srfi-27) ; Sources of Random Bits

  #:use-module (rnrs conditions)

  #:use-module (ice-9 atomic)
  #:use-module (ice-9 match)

  #:export (eris-coap-block-reducer
	    eris-coap-block-ref))

(define* (eris-coap-block-reducer uri #:key (nstart 8) connection)
  "Returns a block reducer that stores blocks to the CoAP store
reachable at the URL @var{uri}.

The @var{nstart} argument can be used to specify the number of
in-flight CoAP request made. If not provided it defaults to 8.

The @var{connection} argument can be used to re-use an existing CoAP
connection. The path part of the URI will still be used to direct
requests to the proper endpoint.

This reducer must be invoked with a running Fibers scheduler
(@pxref{Using Fibers,,,fibers, The Fibers manual})."
  (define opts
    (append
     (make-coap-option 'uri-path (uri-path uri))
     (make-coap-option 'uri-path "blocks")))

  (define error-atomic-box (make-atomic-box #f))

  (define (worker chan completion-channel conn)
    (match (get-message chan)
      (('put block)
       (let ((response (coap.tcp:request conn 'put
					 #:options opts
					 #:payload block)))

	 (if (equal? (coap-message-code response)
		     (make-coap-code 'created))
	     ;; loop
	     (worker chan completion-channel conn)

	     ;; TODO error handling
	     (begin
	       (atomic-box-set! error-atomic-box response)
	       (worker chan completion-channel conn)))))

      ('stop (put-message completion-channel #t))))

  (define chan (make-channel))
  (define completion-channel (make-channel))

  ;; Maybe it would make more sense to use a condition to signal a
  ;; stop. However, waiting for all workers to report their completion has
  ;; the advantage of potential errors being catched or causing some
  ;; trouble.
  (define (stop-workers remaining completion-channel)
    (when (positive? remaining)
      (put-message chan 'stop)
      (get-message completion-channel)
      (stop-workers (1- remaining) completion-channel)))

  (case-lambda
    ;; initialization
    (()
     (let
	 ;; open new connection if not already provided
	 ((conn (or connection
		     (coap.tcp:open-socket-for-uri uri #:nstart nstart))))

       ;; spawn workers
       (for-each
	(lambda (_) (spawn-fiber (lambda () (worker chan completion-channel conn))))
	(iota nstart))

       ;; return the connection
       conn))

    ;; completion
    ((conn)
     (stop-workers nstart completion-channel)

     ;; close connection if we opened it
     (unless connection (close-port conn))

     (not (and=> (atomic-box-ref error-atomic-box)
		 (lambda (resp)
		   (raise
		    (condition
		     (make-error)
		     (make-message-condition
		      "Error while putting block to remote CoAP endpoint.")
		     (make-irritants-condition resp)))))))

    ((conn ref-block)
     (put-message chan `(put ,(cdr ref-block)))
     conn)))

(define (%eris-coap-block-ref port uri ref)
  (let* ((path-options (append
			(make-coap-option 'uri-path (uri-path uri))
			(make-coap-option 'uri-path "blocks")))

	 ;; send request
	 (resp (coap.tcp:request port 'get
				 #:options (cons
					    (make-coap-option 'uri-query ref)
					    path-options))))

    (if (and (coap-message? resp)
	     (equal? (coap-message-code resp)
		     (make-coap-code 'content)))
	(coap-message-payload resp)
	#f)))


(define* (eris-coap-block-ref uri ref #:key connection)
  "Retrieve a block from an ERIS CoAP store reachable at @var{uri}.

An existing CoAP TCP connection can be provided with @var{connection}.

This reducer must be invoked with a running Fibers scheduler
(@pxref{Using Fibers,,,fibers, The Fibers manual})."
  (let* ((conn
	  (if connection
	      connection
	      (coap.tcp:open-socket-for-uri uri)))
	 (block (%eris-coap-block-ref conn uri ref)))

    ;; close connection if we created it
    (unless connection (close-port connection))

    ;; return the block
    block))
