; SPDX-FileCopyrightText: 2023 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (eris sqlite)

  #:use-module (sqlite3)

  #:use-module (srfi srfi-171) ; Transducers

  #:export (eris-sqlite-open
	    eris-sqlite-create-block-table

	    eris-sqlite-ref
	    eris-sqlite-put
	    
	    eris-sqlite-block-reducer))


(define (eris-sqlite-create-block-table db)
  "Ensures that the @code{eris_block} table in the SQLite database
@var{db} is created."
  (sqlite-exec db "CREATE TABLE IF NOT EXISTS eris_block (
   block_id INTEGER PRIMARY KEY,
   ref BLOB UNIQUE,
   block BLOB
);"))

(define* (eris-sqlite-open filename #:key (read-only? #f))
  "Opens the SQLite database at URI @var{filename} and ensures that
the @code{eris_block} table is created. Returns the opened database.

If @var{read-only?} is set the database is opened in read-only
mode. For more advanced options use @code{sqlite-open} and
@code{eris-sqlite-create-block-table} directly."
  (let* ((flags (if read-only?
		    (logior SQLITE_OPEN_READONLY
			    SQLITE_OPEN_URI)
		    (logior SQLITE_OPEN_READWRITE 
			    SQLITE_OPEN_CREATE
			    SQLITE_OPEN_URI)))
	 (db (sqlite-open filename flags)))
    (unless read-only?
      (eris-sqlite-create-block-table db))
    db))

(define (eris-sqlite-ref db ref)
  "Get the block for reference @var{ref} from the SQLite database
@var{db}."
  (let* ((stmt (sqlite-prepare
		db
		"SELECT block FROM eris_block WHERE ref = ?"
		#:cache? #t))
	 (_ (sqlite-bind stmt 1 ref))
	 (v (sqlite-step stmt)))
    (sqlite-finalize stmt)
    (if (vector? v)
	(vector-ref v 0)
	#f)))

(define (eris-sqlite-put db ref block)
  "Store the block @var{block} with reference @var{ref} in the SQLite
database @var{db}."
  (let* ((stmt (sqlite-prepare
		db
		"INSERT OR IGNORE INTO eris_block (ref, block) VALUES (?,?);"
		#:cache? #t)))
    (sqlite-bind stmt 1 ref)
    (sqlite-bind stmt 2 block)
    (sqlite-step stmt)
    (sqlite-finalize stmt)))

(define* (eris-sqlite-block-reducer path-or-db #:key (blocks-in-transaction 42))
  "Returns a block reducer that stores blocks to the SQLite database
@var{db}.

To improve performance multiple blocks are inserted in a batched
transaction. The argument @var{blocks-in-transaction} specifies how
many blocks will be inserted in a single transaction (defaults to 42)."
  ((tsegment blocks-in-transaction) ; ah my dear transducers how beautiful thou are.
   (case-lambda
     ;; Initialization
     (() (if (sqlite-db? path-or-db)
	     path-or-db
	     (eris-sqlite-open path-or-db)))

     ;; Store a block
     ((db ref-blocks)

      (sqlite-exec db "BEGIN TRANSACTION;")
      (for-each
       (lambda (ref-block)
	 (eris-sqlite-put db (car ref-block) (cdr ref-block)))
       ref-blocks)
      (sqlite-exec db "END TRANSACTION;")

      db)

     ((db)
      ;; Close the DB if we opened it
      (unless (sqlite-db? path-or-db)
	(sqlite-close db))))))
