; SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
; Copyright © 2018 Ludovic Courtès <ludo@gnu.org>
;
; SPDX-License-Identifier: GPL-3.0-or-later

;; This module provides an interface to the IPFS daemons HTTP API for storing
;; and retrieving blocks. This can be used to store blocks of ERIS encoded content.
;; 
;; See also the IPFS API documentation:
;; https://docs.ipfs.io/reference/http/api/#api-v0-block-put
;; 
;; Parts are taken from guix/ipfs.scm in the wip-ipfs-substitutes branch of the
;; GNU Guix repository.

(define-module (eris blocks ipfs)

  #:use-module (eris utils base32)

  #:use-module (sodium generichash)

  #:use-module (json)

  #:use-module (web uri)
  #:use-module (web client)
  #:use-module (web response)

  #:use-module (srfi srfi-71)
  
  #:use-module (rnrs io ports)
  #:use-module (rnrs bytevectors)

  #:export (%ipfs-base-url

	    eris-blocks-ipfs-reducer
            eris-blocks-ipfs-ref))


;; CID encoding

;; Multicodec codes (https://github.com/multiformats/multicodec/blob/master/table.csv) 
(define multicodec-raw-code #x55)
(define multicodec-blake2b-256-code #xb220)

(define (blake2b-256->binary-cid hash)
  "Encode a Blake2b-256 hash as binary CID"
  (call-with-values
      (lambda () (open-bytevector-output-port))
    (lambda (port get-bytevector)
      ;; CID version
      (put-u8 port 1)
      ;; multicoded content-type
      (put-u8 port multicodec-raw-code)
      ;; set multihash to blake2b-256. This is the manually encoded varint of 0xb220
      (put-u8 port 160) (put-u8 port 228) (put-u8 port 2)
      ;; set hash lenght
      (put-u8 port 32)
      ;; and finally the hash itself
      (put-bytevector port hash)

      ;; finalize and get the bytevector
      (get-bytevector))))

(define (binary-cid->cid bcid)
  "Encode a binary CID as Base32 encoded CID"
  ;; 'b' is the multibsae code for base32
  (string-append "b"
                 ;; the IPFS daemon uses lower-case, so to be consistent we also.
                 (string-downcase
                  ;; base32 encode the binary cid
                  (base32-encode bcid))))

(define blake2b-256->cid
  (compose binary-cid->cid blake2b-256->binary-cid))


;; IPFS API

(define %ipfs-base-url
  ;; URL of the IPFS gateway.
  (make-parameter "http://localhost:5001"))


(define* (call url decode #:optional (method http-post)
               #:key body (false-if-404? #t) (headers '()))
  "Invoke the endpoint at URL using METHOD.  Decode the resulting JSON body
using DECODE, a one-argument procedure that takes an input port; when DECODE
is false, return the input port.  When FALSE-IF-404? is true, return #f upon
404 responses."
  (let* ((response port
                   (method url #:streaming? #t
                           #:body body

                           ;; Always pass "Connection: close".
                           #:keep-alive? #f
                           #:headers `((connection close)
                                       ,@headers))))
    (cond ((= 200 (response-code response))
           (if decode
               (let ((result (decode port)))
                 (close-port port)
                 result)
               port))
          ((and false-if-404?
                (= 404 (response-code response)))
           (close-port port)
           #f)
          (else
           (close-port port)
           (throw 'ipfs-error url response)))))

(define %multipart-boundary
  ;; XXX: We might want to find a more reliable boundary.
  (string-append (make-string 24 #\-) "2698127afd7425a6"))

(define (bytevector->form-data bv port)
  "Write to PORT a 'multipart/form-data' representation of BV."
  (display (string-append "--" %multipart-boundary "\r\n"
                          "Content-Disposition: form-data\r\n"
                          "Content-Type: application/octet-stream\r\n\r\n")
           port)
  (put-bytevector port bv)
  (display (string-append "\r\n--" %multipart-boundary "--\r\n")
           port))

(define (ipfs-block-put bv)
  "Store a block on IPFS and return the CID of the block"
  (call (string-append (%ipfs-base-url)
                       "/api/v0/block/put"
                       "?format=raw&mhtype=blake2b-256")
        (lambda (port) (assoc-ref (json->scm port) "Key"))
        #:headers `((content-type
                     . (multipart/form-data
                        (boundary . ,%multipart-boundary))))
        #:body (call-with-bytevector-output-port
                (lambda (port) (bytevector->form-data bv port)))))

(define (ipfs-block-get cid)
  "Get a block from IPFS via the HTTP API"
  (call (string-append (%ipfs-base-url)
		       "/api/v0/block/get"
		       "?arg=" cid)
	get-bytevector-all))

;; ERIS block reducer

(define eris-blocks-ipfs-reducer
  (case-lambda
    ;; initialization. Nothing to do here. In an improved implementation we
    ;; might create a single HTTP connection and reuse it for all blocks.
    (() '())

    ;; Completion. Again, nothing to do.
    ((_) 'done)

    ;; store a block
    ((_ ref-block)
     ;; ref-block is a pair consisting of the reference to the block and the block itself.
     (ipfs-block-put (cdr ref-block)))))

(define (eris-blocks-ipfs-ref ref)
  "Dereference a block from IPFS"
  (ipfs-block-get (blake2b-256->cid ref)))

