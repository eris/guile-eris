; SPDX-FileCopyrightText: 2020, 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

;; This module implements the Encoding for Robust Immutable Storage (ERIS)
;;
;; See http://purl.org/eris

(define-module (eris)
  #:use-module (eris encoder)
  #:use-module (eris decoder)
  #:use-module (eris read-capability)
  #:use-module (eris utils buffer)

  #:use-module (srfi srfi-34) ; Exception handling
  #:use-module (srfi srfi-35) ; Conditions
  #:use-module (srfi srfi-71) ; Extended let-syntax for multiple values
  #:use-module (srfi srfi-171) ; Transducers
  #:use-module (srfi srfi-171 meta)

  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:export (eris-encode
	    open-eris-output-port

	    eris-decode
	    open-eris-input-port
	    eris-transduce

	    %null-convergence-secret
	    %eris-spec-version))

;; Encoding

(define (->port readable)
  "Helper to read content as port"
  (cond
   ((port? readable) readable)
   ((bytevector? readable) (open-bytevector-input-port readable))
   ((string? readable) (->port (string->utf8 readable)))))

(define (block-size-in-bytes block-size)
  (cond
   ((eq? block-size 'large) 32768)
   ((eq? block-size 'small) 1024)
   ((eqv? block-size 32768) 32768)
   ((eqv? block-size 1024) 1024)
   (else (raise (condition
		 (&message (message "Invalid block size"))
		 (&error))))))

(define %null-convergence-secret (make-bytevector 32 0))

(define* (eris-encode readable #:key
		      (block-size 'large)
		      (block-reducer reverse-rcons)
		      convergence-secret)
  "Encode content from a port, string or bytevector and reduce blocks
into @code{block-reducer}. Returns the read-capability as URI and the
reduced blocks."
  (let* (;; transform input to a input port
	 (port (->port readable))

	 ;; block size in bytes
	 (block-size (block-size-in-bytes block-size))

	 ;; initialize encoder
	 (encoder (eris-encoder-init #:block-size block-size
				     #:block-reducer block-reducer
				     #:convergence-secret convergence-secret)))

    ;; read from port in block-size chunks and encode
    (let loop ((bv (get-bytevector-n port block-size)))
      (unless (eof-object? bv)
	(eris-encoder-update encoder bv)
	(loop (get-bytevector-n port block-size))))

    (let* ((read-capability blocks (eris-encoder-finalize encoder)))
      (values
       (eris-read-capability->string read-capability)
       blocks))))

(define* (open-eris-output-port #:key (block-size 'large)
				(block-reducer reverse-rcons)
				convergence-secret)

  ;; initializ encoder
  (define encoder
    (eris-encoder-init #:block-size (block-size-in-bytes block-size)
		       #:block-reducer block-reducer
		       #:convergence-secret convergence-secret))

  (define port (make-custom-binary-output-port
		"eris-output-port"
		(lambda (source offset count)
		  (if (eris-encoder? encoder)

		      ;; copy input to a new bytevector and update encoder
		      (let ((bv (make-bytevector count)))
			(bytevector-copy! source offset bv 0 count)
			(eris-encoder-update encoder bv)
			count)

		      ;; nothing written
		      0))
		#f #f #f))

  ;; set port buffer size to block-size
  (setvbuf port 'block (block-size-in-bytes block-size))

  (values
   port
   (lambda ()

     ;; close port (causes buffer to be flushed)
     (close-port port)

     (let ((read-capability blocks (eris-encoder-finalize encoder)))
       ;; set encoder to false to prevent further writes to port
       (set! encoder #f)
       ;; return the read-capability and blocks
       (values (eris-read-capability->string read-capability) blocks)))))

;; Decoding

(define* (open-eris-input-port read-capability #:key block-ref)

  (let ((read-capability (->eris-read-capability read-capability)))

    (unless (eris-read-capability? read-capability)
      (raise (condition
	      (&message (message "invalid ERIS read-capability"))
	      (&error))))

    ;; initialize the decoder
    (define decoder (eris-decoder-init read-capability #:block-ref block-ref))

    (make-custom-binary-input-port
     "eris-input-port"
     (lambda (target target-start len)
       (let ((c new-decoder
		(eris-decoder-read! decoder target target-start len)))

	 (set! decoder new-decoder)
	 c))
     (lambda () (eris-decoder-position decoder))
     (lambda (pos)
       (let* ((new-decoder (eris-decoder-seek decoder pos))
	      (new-pos (eris-decoder-position new-decoder)))
	 (set! decoder new-decoder)
	 (when (< new-pos pos)
	     (set! decoder new-decoder)
	     (raise (condition
		     (&message (message "seek out of range"))
		     (&serious))))))
     (lambda () #t))))

(define (eris-decoder-read decoder n)
  (let* ((bv (make-bytevector n))
	 (c decoder (eris-decoder-read! decoder bv 0 n)))
    (if (< c n)
	(let ((cbv (make-bytevector c)))
	  ;; copy the read bytes into a new bytevector
	  (bytevector-copy! bv 0 cbv 0 c)
	  ;; return the right-sized bytevector and updated decoder
	  (values cbv decoder))
	(values bv decoder))))

(define* (eris-transduce xform f read-capability #:key block-ref)
  "Decode ERIS encoded content by reducing it using the reducer
@var{f} applied to the transducer @var{xform}."

  (let* (;; decode read-capability
	 (read-capability (->eris-read-capability read-capability))
	 (block-size (eris-read-capability-block-size read-capability))

	 ;; initialize decoder
	 (decoder (eris-decoder-init read-capability
				     #:block-ref block-ref))

	 ;; initialize the transducer
	 (xf (xform f))

	 ;; initial read
	 (bv decoder (eris-decoder-read decoder block-size)))

    (let loop ((acc (xf (xf) bv))
	       (bv bv)
	       (decoder decoder))

      ;; reached end of encoded content or reducer finished early
      (if (or (< (bytevector-length bv) block-size)
	      (reduced? acc))

	  ;; apply the completion on accumulator
	  (xf (if (reduced? acc) (unreduce acc) acc))

	  ;; eof not reached. Read next block and loop
	  (let ((bv decoder
		    (eris-decoder-read decoder block-size)))
	    (loop (xf acc bv) bv decoder))))))

(define* (eris-decode read-capability #:key block-ref)
  (eris-transduce (tmap identity)
		  rbytevector
		  read-capability
		  #:block-ref block-ref))

(define %eris-spec-version "1.0.0")
