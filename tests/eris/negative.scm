; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests eris negative)
  #:use-module (eris)
  #:use-module (eris utils base32)

  #:use-module (ice-9 ftw)

  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-180))

(include "test-vector.scm")

(define test-vector-directory "./test-vectors/")

(define test-vector-file-names
  (filter
   (lambda (filename)
     (and (string-prefix? "eris-test-vector-negative" filename)))
   (scandir test-vector-directory)))

(define (read-test-vector filename)
  (call-with-input-file (string-append test-vector-directory filename)
    (lambda (port) (obj->eris-test-vector (json-read port)))))

(define (test-vector-block-ref test-vector)
  (lambda (ref)
    (cond
     ((assoc-ref (eris-test-vector-blocks test-vector)
		 (string->symbol (base32-encode ref)))
      => base32-decode))))

(test-begin "eris (negative)")

(for-each
 (lambda (test-vector-file-name)
   (let ((test-vector (read-test-vector test-vector-file-name)))
     (test-error #t
		 (eris-decode (eris-test-vector-urn test-vector)
			      #:block-ref
			      (test-vector-block-ref test-vector)))))
 test-vector-file-names)

(test-end "eris (negative)")
