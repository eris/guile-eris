; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests eris delimited-continuation)
  #:use-module (eris)

  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 control)

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)

  #:use-module (sodium stream)
  #:use-module (sodium generichash))


;; This is a small example using delimited continuations when decoding
;; ERIS content. In this test we use a block-ref function that instead
;; of returning a bock directly, returns it in a call-back.
;;
;; The same mechanism is used when decoding content with asynchronous
;; libraries such as fibers or goblins.

(define null-nonce (make-bytevector 12 0))

(define* (pseudo-random-bytevector
	  length
	  #:key (seed "Encoding for Robust Immutable Storage"))
  (crypto-stream-chacha20-ietf
   #:clen length
   #:nonce null-nonce
   #:key (crypto-generichash (string->utf8 seed) #:out-len 32)))

(test-begin "eris (delimited-coninuations)")

(let* ((content (pseudo-random-bytevector (* 1024 1024 4)))
       (read-capability
       blocks
       (eris-encode content
		    #:block-size 'large
		    #:convergence-secret %null-convergence-secret)))

  ;; a block-ref that returns the block via a callback
  (define (block-ref-cb ref cb)
    (cb (assoc-ref blocks ref)))

  ;; We use the shift and reset operators as they reinstate the prompt
  ;; on continuation. This is necessary as multiple blocks need to be
  ;; decoded. See the section on Prompts in the Guile Manual and dive
  ;; into Oleg Kiselyov’s wonderful site (http://okmij.org/ftp/).
  (define decoded
    (reset
     (eris-decode
      read-capability
      #:block-ref (lambda (ref) (shift k (block-ref-cb ref k))))))

  (test-assert "decoded bytevector is same as initial bytevector"
    (bytevector=? content decoded)))

(test-end "eris (delimited-coninuations)")

