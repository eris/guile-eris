; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

;; Record type for holding test vectors

(use-modules
 (eris read-capability)
 (eris utils base32)

 (srfi srfi-9)
 (srfi srfi-71))

(define-record-type <eris-test-vector>
  (make-eris-test-vector
   id
   spec-version
   type
   name
   description
   content
   convergence-secret
   block-size
   read-capability
   urn
   blocks)

  eris-test-vector?

  (id eris-test-vector-id)
  (spec-version eris-test-vector-spec-version)
  (type eris-test-vector-type)
  (name eris-test-vector-name)
  (description eris-test-vector-description)

  (content eris-test-vector-content)
  (convergence-secret eris-test-vector-convergence-secret)
  (block-size eris-test-vector-block-size)

  (read-capability eris-test-vector-read-capability)
  (urn eris-test-vector-urn)
  (blocks eris-test-vector-blocks))

;; "Reducer that stores blocks into an alist of base32 encoded values
;; so that the blocks can be directly exported to JSON."
(define rtest-vector-blocks
  (case-lambda
    (() '())
    ((result) result)
    ((result ref-block)
     (cons
      (cons (string->symbol (base32-encode (car ref-block)))
            (base32-encode (cdr ref-block)))
      result))))

(define (make-positive-eris-test-vector id name description content
					convergence-secret block-size)
  (let* ((read-capability blocks (eris-encode content
					      #:block-reducer rtest-vector-blocks
					      #:block-size block-size
					      #:convergence-secret convergence-secret)))
    (make-eris-test-vector
     id
     eris-spec-version
     "positive"
     name description
     content
     convergence-secret block-size
     (->eris-read-capability read-capability)
     read-capability
     blocks)))

(define (read-capability->obj read-capability)
  `((block-size . ,(eris-read-capability-block-size read-capability))
    (level . ,(eris-read-capability-level read-capability))
    (root-reference . ,(base32-encode (eris-read-capability-root-reference read-capability)))
    (root-key . ,(base32-encode (eris-read-capability-root-key read-capability)))))

(define (obj->read-capability obj)
  (make-eris-read-capability
   (assq-ref obj 'block-size)
   (assq-ref obj 'level)
   (base32-decode (assq-ref obj 'root-reference))
   (base32-decode (assq-ref obj 'root-key))))

(define (eris-test-vector->obj test-vector)
  (filter
   (lambda (pair) (cdr pair))
   `((id . ,(eris-test-vector-id test-vector))
     (type . ,(eris-test-vector-type test-vector))
     (spec-version . ,(eris-test-vector-spec-version test-vector))
     (name . ,(eris-test-vector-name test-vector))
     (description . ,(eris-test-vector-description test-vector))
     (content . ,(if (eris-test-vector-content test-vector)
		     (base32-encode (eris-test-vector-content test-vector))
		     #f))
     (convergence-secret . ,(if (eris-test-vector-convergence-secret test-vector)
				(base32-encode (eris-test-vector-convergence-secret test-vector))
				#f))
     (block-size . ,(eris-test-vector-block-size test-vector))
     (read-capability . ,(read-capability->obj (eris-test-vector-read-capability test-vector)))
     (urn . ,(eris-test-vector-urn test-vector))
     (blocks . ,(eris-test-vector-blocks test-vector)))))

(define (obj->eris-test-vector obj)
  (make-eris-test-vector
   (assq-ref obj 'id)
   (assq-ref obj 'spec-version)
   (assq-ref obj 'type)
   (assq-ref obj 'name)
   (assq-ref obj 'description)
   (cond ((assq-ref obj 'content) => base32-decode)
	 (else #f))
   (cond ((assq-ref obj 'convergence-secret) => base32-decode)
	 (else #f))
   (assq-ref obj 'block-size)
   (obj->read-capability (assq-ref obj 'read-capability))
   (assq-ref obj 'urn)
   (assq-ref obj 'blocks)))
