; SPDX-FileCopyrightText: 2020, 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests eris positive)
  #:use-module (eris)
  #:use-module (eris utils base32)

  #:use-module (ice-9 ftw)


  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)
  #:use-module (srfi srfi-171)
  #:use-module (srfi srfi-180))

(include "test-vector.scm")

(define test-vector-directory "./test-vectors/")

(define test-vector-file-names (filter
                                (lambda (filename)
                                  (and (string-prefix? "eris-test-vector-positive" filename)))
                                (scandir test-vector-directory)))

(define (read-test-vector filename)
  (call-with-input-file (string-append test-vector-directory filename)
    (lambda (port) (obj->eris-test-vector (json-read port)))))

(define (test-vector-block-ref test-vector)
  (lambda (ref)
    (base32-decode
     (assoc-ref (eris-test-vector-blocks test-vector)
		(string->symbol (base32-encode ref))))))

(define* (eris-encode->string readable #:key block-size convergence-secret)
  (let* ((urn blocks (eris-encode readable
				  #:block-size block-size
				  #:convergence-secret convergence-secret
				  #:block-reducer rcount)))
    urn))

(test-begin "eris (positive)")

(for-each
 (lambda (test-vector-file-name)
   (let ((test-vector (read-test-vector test-vector-file-name)))

     (display test-vector-file-name)

     (test-equal "content encodes to specified URN"
       (eris-test-vector-urn test-vector)
       (eris-encode->string (eris-test-vector-content test-vector)
			    #:convergence-secret (eris-test-vector-convergence-secret test-vector)
			    #:block-size (eris-test-vector-block-size test-vector)))

     (test-assert "content can be decoded from blocks and URN"
       (bytevector=? (eris-test-vector-content test-vector)
                     (eris-decode (eris-test-vector-urn test-vector)
				  #:block-ref (test-vector-block-ref test-vector))))))
 
 test-vector-file-names)

(test-end "eris (positive)")
