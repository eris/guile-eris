; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests eris large)
  #:use-module (eris encoder)
  #:use-module (eris read-capability)

  #:use-module (sodium stream)
  #:use-module (sodium generichash)

  #:use-module (rnrs bytevectors)

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)
  #:use-module (srfi srfi-171))


(define* (crypto-stream-chacha20-ietf-generator #:key block-size nonce key (ic 0))
  "Returns a generator that emits the ChaCha20 stream"
  (let ( ;; XOR with null byte sequence to get the ChaCha20 stream
        (message (make-bytevector block-size 0))
        ;; Internal block size of ChaCha20 is 64 bytes, increment by ic-increment every invocation
        (ic-increment (/ block-size 64)))
    (lambda ()
      (let ((c (crypto-stream-chacha20-ietf-xor-ic #:message message #:nonce nonce #:ic ic #:key key)))
        (set! ic (+ ic ic-increment))
        c))))

(define null-nonce (make-bytevector 12 0))
(define %null-convergence-secret (make-bytevector 32 0))

(define* (size->block-count size #:key (block-size 32768))
  (/ size block-size))


(define* (teris-encode #:key block-size
		       (convergence-secret (make-bytevector 32 0)))
  (lambda (reducer)
    (let ((encoder (eris-encoder-init #:block-size block-size
				      #:convergence-secret convergence-secret
				      #:block-reducer reducer
				      #:identity #f)))
      (case-lambda
	;; run the reducer to get identity and set as result in encoder
	(() (let ((identity (reducer)))
	      (set-eris-encoder-result! encoder identity)
	      identity))

	;; finalizer
	((result)
	 (let* ((_ (set-eris-encoder-result! encoder result))
		(read-capability result (eris-encoder-finalize encoder #:finalize-reducer? #f)))
	   ;; reduce the read-capability and finalize
	   (reducer (reducer result read-capability))))

	;; set result as result in encoder, encode input and extract updated result
	((result input)
	 (begin
	   (set-eris-encoder-result! encoder result)
	   (eris-encoder-update encoder input)
	   (eris-encoder-result encoder)))))))

(test-begin "eris (large)")

(test-equal "100MiB (block size 1KiB)"
  "urn:eris:BIC6F5EKY2PMXS2VNOKPD3AJGKTQBD3EXSCSLZIENXAXBM7PCTH2TCMF5OKJWAN36N4DFO6JPFZBR3MS7ECOGDYDERIJJ4N5KAQSZS67YY"
  (eris-read-capability->string
   (car
    (generator-transduce
     (compose (ttake (size->block-count (* 1024 1024 100) #:block-size 1024))
              (teris-encode #:convergence-secret %null-convergence-secret
			    #:block-size 1024)
              (tfilter eris-read-capability?))
     rcons
     ;; Generate a stream of random blocks using the ChaCha20 stream
     (crypto-stream-chacha20-ietf-generator
      #:block-size 1024
      #:nonce null-nonce
      #:key (crypto-generichash (string->utf8 "100MiB (block size 1KiB)"))
      #:ic 0)))))

(test-equal "1GiB (block size 32KiB)"
  "urn:eris:B4BL4DKSEOPGMYS2CU2OFNYCH4BGQT774GXKGURLFO5FDXAQQPJGJ35AZR3PEK6CVCV74FVTAXHRSWLUUNYYA46ZPOPDOV2M5NVLBETWVI"
  (eris-read-capability->string
   (car
    (generator-transduce
     (compose (ttake (size->block-count (* 1024 1024 1024)))
              (teris-encode #:convergence-secret %null-convergence-secret
			    #:block-size 32768)
	      (tfilter eris-read-capability?))
     rcons
     ;; Generate a stream of random blocks using the ChaCha20 stream
     (crypto-stream-chacha20-ietf-generator
      #:block-size 32768
      #:nonce null-nonce
      #:key (crypto-generichash (string->utf8 "1GiB (block size 32KiB)"))
      #:ic 0)))))

;; This test takes almost 20 minutes on my machine. Nice to run once in a while, not always.
;; (test-equal "256GiB (block size 32KiB)"
;;   "urn:eris:B4B5DNZVGU4QDCN7TAYWQZE5IJ6ESAOESEVYB5PPWFWHE252OY4X5XXJMNL4JMMFMO5LNITC7OGCLU4IOSZ7G6SA5F2VTZG2GZ5UCYFD5E"
;;   (eris-read-capability->string
;;    (car
;;     (generator-transduce
;;      (compose (ttake (size->block-count (* 1024 1024 1024 256)))
;; 	      (teris-encode #:convergence-secret %null-convergence-secret
;; 			    #:block-size 32768)
;; 	      (tfilter eris-read-capability?))
;;      rcons
;;      ;; Generate a stream of random blocks using the ChaCha20 stream
;;      (crypto-stream-chacha20-ietf-generator
;;       #:block-size 32768
;;       #:nonce null-nonce
;;       #:key (crypto-generichash (string->utf8 "256GiB (block size 32KiB)"))
;;       #:ic 0)))))

(test-end "eris (large)")
