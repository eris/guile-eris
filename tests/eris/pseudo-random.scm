; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests eris pseudo-random)
  #:use-module (eris)
  #:use-module (eris read-capability)

  #:use-module (sodium stream)
  #:use-module (sodium generichash)

  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-171))

(define null-nonce (make-bytevector 12 0))
(define %null-convergence-secret (make-bytevector 32 0))

(define* (pseudo-random-bytevector
	  length
	  #:key (seed "Encoding for Robust Immutable Storage"))
  (crypto-stream-chacha20-ietf
   #:clen length
   #:nonce null-nonce
   #:key (crypto-generichash (string->utf8 seed) #:out-len 32)))

(define content-sizes-small
  (list 0
        1
        8
        32
        1023
        1024
        (* 10 1024)
        (* 32 1024)
        (* 1024 1024)))


(test-begin "eris (pseudo-random, 1KiB block-size)")

(for-each
 (lambda (l)

   (format #t "lenght (1KiB): ~a\n" l)

   (let ((bv (pseudo-random-bytevector l)))

     (let-values (((urn blocks) (eris-encode bv
                                             #:block-reducer rcons
                                             #:convergence-secret %null-convergence-secret
                                             #:block-size 'small)))

       (test-assert "decoded bytevector is same as initial bytevector"
         (bytevector=?
          bv
          (eris-decode
	   urn
	   #:block-ref (lambda (ref) (assoc-ref blocks ref))))))))

 content-sizes-small)

(test-end "eris (pseudo-random, 1KiB block-size)")

(define content-sizes-large
  (list 0
        1
        8
        32
        1023
        1024
        424242
        31230924
        (* 10 1024)
        (* 32 1024)
        (* 1024 1024)
        (* 1024 1024 32)
        (* 1024 1024 100)))
(test-begin "eris (pseudo-random, 32KiB block-size)")

(for-each
 (lambda (l)

   (format #t "lenght (32KiB): ~a\n" l)

   (let ((bv (pseudo-random-bytevector l)))

     (let-values (((urn blocks) (eris-encode bv
                                             #:block-reducer rcons
                                             #:convergence-secret %null-convergence-secret
                                             #:block-size 'large)))

       (test-assert "decoded bytevector is same as initial bytevector"
         (bytevector=?
          bv
          (eris-decode
	   urn
	   #:block-ref (lambda (ref) (assoc-ref blocks ref))))))))

 content-sizes-large)

(test-end "eris (pseudo-random, 32KiB block-size)")
