; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests eris random-access)
  #:use-module (eris)
  #:use-module (eris decoder)
  #:use-module (eris read-capability)

  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:use-module (quickcheck)
  #:use-module (quickcheck arbitrary)
  #:use-module (quickcheck generator)
  #:use-module (quickcheck property)
  #:use-module (quickcheck rng)

  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71))

;; The choose-bytevector from guile-quickcheck is too slow to generate
;; large random bytevectors. This version uses the system random
;; generator (with random seed taken from the quickcheck rng). With this
;; large bytevectors (many MiB) can be generated.
(define (choose-bytevector* size)
  (generator-bind
   (choose-integer 0 #xffff)
   (lambda (seed)
     (let ((state (seed->random-state seed))
	   (bv (make-bytevector size)))
       (let loop ((i 0))
	 (when (< i size)
	   (bytevector-u8-set! bv i (random 255 state))
	   (loop (1+ i))))
       (generator-return bv)))))

(define $bytevector*
  (arbitrary
   (gen (sized-generator choose-bytevector*))
   (xform (lambda (x gen) x))))


;; VHash block storage

(define rvhash-cons
  (case-lambda
    (() vlist-null)
    ((result) result)
    ((result ref-block)
     (vhash-cons (car ref-block) (cdr ref-block) result))))

(define (vhash-block-ref blocks)
  (lambda (ref) (cdr (vhash-assoc ref blocks))))

;; Encode/Decode test

;; Currently not used. This was just a warm-up with guile-quickcheck.

(define (encode-decode-equal? bv)
  (let* ((read-capability blocks
			  (eris-encode bv
				       ;; reduce blocks to a vhash
				       #:block-reducer rvhash-cons))
	 (decoded (eris-decode
		   read-capability #:block-ref (vhash-block-ref blocks))))
    (bytevector=? bv decoded)))

(define ensure-encode-decode-equal
  (property ((bv $bytevector*))
    (encode-decode-equal? bv)))

;; (quickcheck ensure-encode-decode-equal)


;; Random-Access tests

(define (test-seq offset n)
  "Generates n bytes starting at offset from test sequence"
  (let loop ((bv (make-bytevector n))
	     (i 0))
    (if (< i n)
	(begin
	  (bytevector-u8-set! bv i (hash (+ offset i) 255))
	  (loop bv (1+ i)))
	bv)))

(define* (eris-encode-test-seq n #:key (block-size 32768))
  (let ((port finalize
	      (open-eris-output-port
	       #:block-size block-size
	       #:block-reducer rvhash-cons
	       #:convergence-secret %null-convergence-secret)))
    (let loop ((i 0))
      (if (< i n)
	  (let ((c (min block-size (- n i))))
	    (put-bytevector port (test-seq i c))
	    (loop (+ i c)))
	(finalize)))))

;; Note this is not so nice construction of arbitraries... TODO clean up.

(define ($content-size max)
  (arbitrary
   (gen (choose-integer 512 max))
   (xform generator-variant)))

(define $small-natural
  (arbitrary
   (gen (choose-integer 0 1024))
   (xform generator-variant)))

(define ($reads max) 
  ($list ($pair ($content-size max) $small-natural)))

(define* (ensure-random-access
	  #:key (block-size 32768)
	  (max-content-size (* 1024 1024 10)))

  (property ((size ($content-size max-content-size))
	     (reads ($reads max-content-size)))

    (format #t "Testing random access. test-sequence-size: ~a bytes, block-size: ~a:\n"
	    size block-size)

    (let*

	;; Encode test sequence
	((read-capability
	  blocks (eris-encode-test-seq size #:block-size block-size))
	 ;; Open input port
	 (port (open-eris-input-port
		read-capability
		#:block-ref (vhash-block-ref blocks))))

      ;; Go over all the reads
      (every
       (lambda (read)

	 ;; Some hackery to make sure reads are in bound
	 (let* ((n (min (cdr read) size))
		(offset (max 0 (- (modulo (car read) size) 1024))))

	   (format #t "\tReading ~a bytes at ~a.\n" n offset)

	   ;; Seek port to position
	   (set-port-position! port offset)

	   ;; Compare read bytes with expected test sequence
	   (let ((expected (test-seq offset n))
		 (read (get-bytevector-n port n)))
	     (bytevector=? expected read))))
       reads))))


;; Run tests

(test-begin "eris (random-access, 1KiB block-size)")

(configure-quickcheck
 ;; run 10 tests
 (stop? (lambda (success-count discard-count) (>= success-count 10)))
 ;; set size to something between 0 and 100
 (size (lambda (i) (hash i 100))))

;; small block size up to 1 MiB
(quickcheck (ensure-random-access #:max-content-size (* 1024 1024)
				  #:block-size 1024))

(test-end "eris (random-access, 1KiB block-size)")

(test-begin "eris (random-access, 32KiB block-size)")

(configure-quickcheck
 ;; run 3 tests
 (stop? (lambda (success-count discard-count) (>= success-count 3)))
 ;; set size to something between 0 and 1000
 (size (lambda (i) (hash i 1000))))

;; with large block size up to 64 MiB
(quickcheck (ensure-random-access #:max-content-size (* 1024 1024 64)
				  #:block-size 32768))

(test-end "eris (random-access, 32KiB block-size)")
