; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix git)
  (guix git-download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages databases)
  (gnu packages license)
  (gnu packages crypto)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(define-public guile-coap
  (package
   (name "guile-coap")
   (version "0.1.0")
   (home-page "https://codeberg.org/eris/guile-coap")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url home-page)
	   (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1xy5wcrx2lf5db38vj1d7x857c3xy7ns9gb3qqwssk47m7s1693h"))))
   (build-system gnu-build-system)
   (native-inputs
    (list autoconf
	  automake
	  pkg-config
	  texinfo))
   (propagated-inputs (list guile-fibers))
   (inputs (list guile-3.0))
   (synopsis "Guile implementation of the Constrained Application
Protocol (CoAP)")
   (description "Guile-CoAP is a Guile Scheme implementtion of the
Constrained Application Protocol (CoAP) as defined by RFC 7252.  CoAP
is a internet application protocol designed for constraine devices.
It offers a request/response semantic similar to HTTP, but also
supports other interaction models such as publish-subscribe.  This
library provides basic message serialization for CoAP over UDP as
defined by RFC 7252 as well as CoAP over TCP as defined by RFC 8323.")
   (license license:gpl3)))

(package
  (name "guile-eris")
  (version "1.0.0")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (arguments '())
  (native-inputs
   (list autoconf
	 automake
	 pkg-config
	 texinfo
	 ;; test dependency
	 guile-srfi-180
	 guile-quickcheck))
  (inputs (list guile-next))
  (propagated-inputs
   (list guile-sodium
	 guile-coap
	 guile-cbor
	 guile-sqlite3))
  (synopsis
   "Guile implementation of Encoding for Robust Immutable Storage (ERIS)")
  (description
   "Guile-ERIS is the reference implementation of the Encoding for Robust
Immutable Storage (ERIS).  ERIS allows arbitrary content to be encoded into
uniformly sized, encrypted blocks that can be reassembled using a short
read-capability.")
  (home-page "https://codeberg.org/eris/guile-eris")
  (license license:gpl3+))

