# Guile-ERIS

Guile implementation of the [Encoding for Robust Immutable Storage (ERIS)](http://purl.org/eris).

This library can be used to encoded and decode content with ERIS. Features include:

- Streaming encoding
- Random-access decoder
- A fiberized CoAP client for working [ERIS CoAP stores](https://eris.codeberg.page/eer/coap.xml)

## Documentation

```
info doc/guile-eris.info
```

An online version is available [here](https://eris.codeberg.page/guile-eris).

## Development

A suitable development environment can be provisioned with Guix:

```
guix shell -D -f guix.scm
```

To build and run the tests:

```
autoreconv -vif
./configure
make
make check
```

### Building HTML documentation

```
make html
```

## Acknowledgments

guile-eris was initially developed as part of the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLnet Foundation](https://nlnet.nl) trough the [NGI0 Discovery Fund](https://nlnet.nl/discovery/). Further development has been supported by the NLnet Foundation through [NGI Assure](https://nlnet.nl/assure/).

## License

[GPL-3.0-or-later](./LICENSES/GPL-3.0-or-later)
